package models

type PatientDiagnosis struct {
	ID          string `json:"id,omitempty"`
	PatientID   string `json:"patient_id,omitempty"`
	DiagnosisID string `json:"diagnosis_id,omitempty"`
	ProcedureID string `json:"procedure_id,omitempty"`
	Time        string `json:"time,omitempty"`
	Comment     string `json:"comment,omitempty"`
	CreatedAt   string `json:"created_at,omitempty"`
	ProcedureName string `json:"procedure_name,omitempty"`
}

type PatientDiagnosisCreate struct {
	PatientID   string `json:"patient_id"`
	DiagnosisID string `json:"diagnosis_id"`
	ProcedureID string `json:"procedure_id"`
	Time        string `json:"time"`
	Comment     string `json:"comment"`
}

type PatientDiagnosisUpdate struct {
	ID          string `json:"id"`
	PatientID   string `json:"patient_id"`
	DiagnosisID string `json:"diagnosis_id"`
	ProcedureID string `json:"procedure_id"`
	UpdatedAt   string `json:"updated_at"`
}

type PatientDiagnosisPrimaryKey struct {
	ID string `json:"id"`
}

type PatientDiagnosisGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type PatientDiagnosisGetListResponse struct {
	PatientDiagnosis []*PatientDiagnosis `json:"data"`
	Count            int                 `json:"total_count"`
}
