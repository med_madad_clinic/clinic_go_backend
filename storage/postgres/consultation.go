package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type consultationRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewConsultationRepo(db *pgxpool.Pool, log logger.LoggerI) *consultationRepo {
	return &consultationRepo{
		db:  db,
		log: log,
	}
}

func (u *consultationRepo) Create(ctx context.Context, req *models.ConsultationCreate) (*models.Consultation, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO consultations (
			id,
			patient_id,
			employee_id,
			date,
			start_time,
			end_time,
			type,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.PatientId, req.EmployeeId, req.Date, req.StartTime, req.EndTime, req.Type, currentTime)
	if err != nil {
		u.log.Error("error is while creating consultation data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ConsultationPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id consultation" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *consultationRepo) GetByID(ctx context.Context, req *models.ConsultationPrimaryKey) (*models.Consultation, error) {
	var (
		query       string
		id          sql.NullString
		patient_id  sql.NullString
		employee_id sql.NullString
		date        sql.NullString
		start_time  sql.NullString
		end_time    sql.NullString
		typee       sql.NullString
		created_at  sql.NullString
		status      sql.NullString
	)

	query = `
		SELECT 
			id,
			patient_id,
			employee_id,
			date,
			start_time,
			end_time,
			type,
			status,
			TO_CHAR(created_at,'dd/mm/yyyy') 
		FROM "consultations"
		WHERE  id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&patient_id,
		&employee_id,
		&date,
		&start_time,
		&end_time,
		&typee,
		&status,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Consultation{
		Id:         id.String,
		PatientId:  patient_id.String,
		EmployeeId: employee_id.String,
		Date:       date.String,
		StartTime:  start_time.String,
		EndTime:    end_time.String,
		Type:       typee.String,
		Status:     status.String,
		CreatedAt:  created_at.String,
	}, nil
}

func (u *consultationRepo) GetList(ctx context.Context, req *models.ConsultationGetListRequest) (*models.ConsultationGetListResponse, error) {
	var (
		resp   = &models.ConsultationGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			patient_id,
			employee_id,
			date,
			start_time,
			end_time,
			type,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "consultations"
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting consultation list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			patient_id  sql.NullString
			employee_id sql.NullString
			date        sql.NullString
			start_time  sql.NullString
			end_time    sql.NullString
			typee       sql.NullString
			created_at  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&patient_id,
			&employee_id,
			&date,
			&start_time,
			&end_time,
			&typee,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Consultation = append(resp.Consultation, &models.Consultation{
			Id:         id.String,
			PatientId:  patient_id.String,
			EmployeeId: employee_id.String,
			Date:       date.String,
			StartTime:  start_time.String,
			EndTime:    end_time.String,
			Type:       typee.String,
			CreatedAt:  created_at.String,
		})
	}
	return resp, nil
}

func (u *consultationRepo) Delete(ctx context.Context, req *models.ConsultationPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE from consultations WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting consultations", logger.Error(err))
		return err
	}

	return nil
}
