package models

type Employee struct {
	Id         string  `json:"id,omitempty"`
	Role_id    string  `json:"role_id,omitempty"`
	Full_name  string  `json:"full_name,omitempty"`
	Phone      string  `json:"phone,omitempty"`
	Birthday   string  `json:"birthday,omitempty"`
	Login      string  `json:"login,omitempty"`
	Password   string  `json:"password,omitempty"`
	Balance    float64 `json:"balance,omitempty"`
	Create_at  string  `json:"create_at,omitempty"`
	Updated_at string  `json:"updated_at,omitempty"`
	Deleted_at string  `json:"deleted_at,omitempty"`
	Order      int     `json:"order,omitempty"`
	Role       string  `json:"role,omitempty"`
}

type EmployeeCreate struct {
	Role_id   string `json:"role_id"`
	Full_name string `json:"full_name"`
	Phone     string `json:"phone"`
	Birthday  string `json:"birthday"`
}

type EmployeeUpdate struct {
	Id        string `json:"id"`
	Full_name string `json:"full_name"`
	Phone     string `json:"phone"`
	Birthday  string `json:"birthday"`
}

type EmployeePrimaryKey struct {
	Id     string  `json:"id"`
	Amount float64 `json:"amount"`
}

type EmployeeGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
	Role   string `json:"role"`
}

type EmployeeGetListResponse struct {
	Count    int         `json:"count"`
	Employee []*Employee `json:"employees"`
}
