package models

type Treatment struct {
	Id           string                 `json:"id,omitempty"`
	DiagnosisId  string                 `json:"diagnosis_id,omitempty"`
	EmployeeId   string                 `json:"employee_id,omitempty"`
	BloodPresure string                 `json:"blood_presure,omitempty"`
	EmployeeName string                 `json:"employee_name,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Count        int                    `json:"count,omitempty"`
	CreatedAt    string                 `json:"created_at,omitempty"`
	UpdatedAt    string                 `json:"updated_at,omitempty"`
	Procedure    TreatmentsProcedure `json:"procedure,omitempty"`
}

type TreatmentCreate struct {
	DiagnosisId  string                      `json:"diagnosis_id"`
	EmployeeId   string                      `json:"employee_id"`
	Count        int                         `json:"count"`
	BloodPresure string                      `json:"blood_presure"`
	Procedure    []*TreatmentProcedureCreate `json:"procedure"`
}

type TreatmentUpdate struct {
	Status string `json:"status"`
	Count  int    `json:"count"`
}

type TreatmentPrimaryKey struct {
	Id string `json:"id"`
}

type TreatmentGetListRequest struct {
	Offset      int    `json:"offset"`
	Limit       int    `json:"limit"`
	Search      string `json:"search"`
	DiagnosisId string `json:"diagnosis_id"`
}

type TreatmentGetListResponse struct {
	Count     int          `json:"count"`
	Treatment []*Treatment `json:"treatment"`
}

type TreatmentsProcedure struct {
	BloodPressure string                `json:"blood_pressure"`
	Procedures    []*TreatmentProcedure `json:"procedures"`
}


type GetProcedureByDiagnosisId struct {
	Procedures []*Procedure `json:"procedures"`
}
