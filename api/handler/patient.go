package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Create Patients godoc
// @ID create_patients
// @Router /clinic/api/v1/patient [POST]
// @Summary Create Patients
// @Description Create Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param Patient body models.PatientCreate true "CreatePatientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreatePatient(c *gin.Context) {
	var (
		patientCreate models.PatientCreate
	)

	err := c.ShouldBindJSON(&patientCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}


	
	

	birthday, err := time.Parse("2006-01-02", patientCreate.Birthday)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	currentDate, err := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	currentYear := currentDate.Year()
	birthYear := birthday.Year()

	if birthday.Month() <= currentDate.Month() {
		if birthday.Month() == currentDate.Month() && birthday.Day() <= currentDate.Day() {
			patientCreate.Age = currentYear - birthYear
		}
		if birthday.Month() == currentDate.Month() && birthday.Day() > currentDate.Day() {
			patientCreate.Age = currentYear - birthYear - 1
		} else {
			patientCreate.Age = currentYear - birthYear
		}
	} else {
		patientCreate.Age = currentYear - birthYear - 1
	}

	resp, err := h.storage.Patient().Create(c.Request.Context(), &patientCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	_,err = h.storage.Consultation().Create(c.Request.Context(), &models.ConsultationCreate{
		PatientId: resp.Id,
		EmployeeId:  patientCreate.EmployeeID,
		Date:       patientCreate.ConsultationDate,
		StartTime:  patientCreate.ArrivalTime,
		EndTime:    patientCreate.LeavingTime,
		Type: patientCreate.Consultation_type,
	})
	if err!=nil{
		h.logger.Error(err.Error() + "  :  " + "error Consultation.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, v := range resp.Url {
		_, err := h.storage.Questionnaire().Create(c.Request.Context(), &models.QuestionnaireCreate{
			Url:        v.Url,
			Patient_id: resp.Id,
		})

		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error Questionnaire.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	h.logger.Info("Create Patient Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Patients godoc
// @ID get_by_id_patients
// @Router /clinic/api/v1/patient/{id} [GET]
// @Summary Get By ID Patients
// @Description Get By ID Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdPatient(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Patient().GetByID(c.Request.Context(), &models.PatientPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Patient Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Patients godoc
// @ID get_list_patients
// @Router /clinic/api/v1/patients [GET]
// @Summary Get List Patients
// @Description Get List Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListPatient(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Patient().GetList(c.Request.Context(), &models.PatientGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPatient Response!")
	c.JSON(http.StatusOK, resp)
}


// GetList Deleted Patients godoc
// @ID get_list_Deleted patients
// @Router /clinic/api/v1/deleted-patients [GET]
// @Summary Get List Deleted Patients
// @Description Get List Deleted Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListDeletedPatient(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Patient().GetListDeletedPatients(c.Request.Context(), &models.PatientGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPatient Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Patients godoc
// @ID update_patients
// @Router /clinic/api/v1/patient/{id} [PUT]
// @Summary Update Patients
// @Description Update Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Patient body models.PatientUpdate true "UpdatePatientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdatePatient(c *gin.Context) {
	var (
		id            = c.Param("id")
		patientUpdate models.PatientUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&patientUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	patientUpdate.Id = id
	rowsAffected, err := h.storage.Patient().Update(c.Request.Context(), &patientUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Patient.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Patient().GetByID(c.Request.Context(), &models.PatientPrimaryKey{Id: patientUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Patient Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Patients godoc
// @ID delete_patients
// @Router /clinic/api/v1/patient/{id} [DELETE]
// @Summary Delete Patients
// @Description Delete Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeletePatient(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Patient().Delete(c.Request.Context(), &models.PatientPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Patient Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}


// GetList Consulation Patients godoc
// @ID get_list_consultation_patients
// @Router /clinic/api/v1/consultation-patients [GET]
// @Summary Get List consultation Patients
// @Description Get List consultation Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListConsultationPatient(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Patient().GetListConsultationPatients(c.Request.Context(), &models.PatientGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPatient Response!")
	c.JSON(http.StatusOK, resp)
}


// GetList Nurse Patients godoc
// @ID get_list_nurse_patients
// @Router /clinic/api/v1/nurse-patients [GET]
// @Summary Get List nurse Patients
// @Description Get List nurse Patients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListNursePatient(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Patient().GetListNursePatients(c.Request.Context(), &models.PatientGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPatient Response!")
	c.JSON(http.StatusOK, resp)
}
