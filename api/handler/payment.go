package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Payments godoc
// @ID create_payments
// @Router /clinic/api/v1/payment [POST]
// @Summary Create Payments
// @Description Create Payments
// @Tags Payment
// @Accept json
// @Payment json
// @Param Payment body models.PaymentCreate true "CreatePaymentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreatePayment(c *gin.Context) {
	var (
		paymentCreate models.PaymentCreate
	)

	err := c.ShouldBindJSON(&paymentCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Payment Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Payment().Create(c.Request.Context(), &paymentCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Payment.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Payment Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Payments godoc
// @ID get_by_id_payments
// @Router /clinic/api/v1/payment/{id} [GET]
// @Summary Get By ID Payments
// @Description Get By ID Payments
// @Tags Payment
// @Accept json
// @Payment json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdPayment(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Payment().GetByID(c.Request.Context(), &models.PaymentPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Payment.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Payment Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Payments godoc
// @ID get_list_payments
// @Router /clinic/api/v1/payments [GET]
// @Summary Get List Payments
// @Description Get List Payments
// @Tags Payment
// @Accept json
// @Payment json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListPayment(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPayment INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPayment INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Payment().GetList(c.Request.Context(), &models.PaymentGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Payment.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPayment Response!")
	c.JSON(http.StatusOK, resp)
}

// Delete Payments godoc
// @ID delete_payments
// @Router /clinic/api/v1/payment/{id} [DELETE]
// @Summary Delete Payments
// @Description Delete Payments
// @Tags Payment
// @Accept json
// @Payment json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeletePayment(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Payment().Delete(c.Request.Context(), &models.PaymentPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Payment.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Payment Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
