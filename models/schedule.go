package models

type Schedule struct {
	Id          string `json:"id,omitempty"`
	Patient_id  string `json:"patient_id,omitempty"`
	Employee_id string `json:"emloyee_id,omitempty"`
	Date        string `json:"start_date,omitempty"`
	Day         string `json:"day,omitempty"`
	Start_time  string `json:"start_time,omitempty"`
	End_time    string `json:"end_time,omitempty"`
	Created_at  string `json:"created_at,omitempty"`
	Status      string `json:"status,omitempty"`
}

type ScheduleCreate struct {
	Patient_id  string `json:"patient_id"`
	Employee_id string `json:"emloyee_id"`
	Date        string `json:"start_date"`
	Day         string `json:"day"`
	Start_time  string `json:"start_time"`
	End_time    string `json:"end_time"`
}

type ScheduelUpdate struct {
	Id          string `json:"id"`
	Employee_id string `json:"emloyee_id"`
	Day         string `json:"day"`
	Start_time  string `json:"start_time"`
	End_time    string `json:"end_time"`
}

type SchedulePrimaryKey struct {
	Id string `json:"id"`
}

type ScheduleGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type ScheduleGetListResponse struct {
	Count    int         `json:"count"`
	Schedule []*Schedule `json:"schedule"`
}

type GetEmployeesScheduleRequest struct {
	Date       string `json:"date"`
	EmployeeId string `json:"employee_id"`
}

type GetEmployeesScheduleResponse struct {
	Schedule []*EmployeeShedule `json:"schedule"`
}

type EmployeeShedule struct {
	EmployeeId   string     `json:"employee_id,omitempty"`
	EmployeeName string     `json:"employee_name,omitempty"`
	Patient      []*Patient `json:"patient,omitempty"`
}

type TodaySchedule struct {
	EmployeeId   string `json:"employee_id,omitempty"`
	EmployeeName string `json:"employee_name,omitempty"`
	PatientId    string `json:"patient_id,omitempty"`
	PatientName  string `json:"patient_name,omitempty"`
	StartTime    string `json:"start_time,omitempty"`
	EndTime      string `json:"end_time,omitempty"`
}

type GetListTodayScheduleResponse struct {
	Schedule []*TodaySchedule `json:"schedule"`
}
