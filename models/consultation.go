package models

type Consultation struct {
	Id          string `json:"id,omitempty"`
	PatientId   string `json:"patient_id,omitempty"`
	EmployeeId  string `json:"employee_id,omitempty"`
	PatientName string `json:"patient_name,omitempty"`
	Gender      string `json:"gender,omitempty"`
	Age         int64  `json:"age,omitempty"`
	Date        string `json:"date,omitempty"`
	StartTime   string `json:"start_time,omitempty"`
	EndTime     string `json:"end_time,omitempty"`
	Type        string `json:"type,omitempty"`
	CreatedAt   string `json:"created_at,omitempty"`
	Status      string `json:"status,omitempty"`
	Order       int    `json:"order,omitempty"`
}

type ConsultationCreate struct {
	PatientId  string `json:"patient_id"`
	EmployeeId string `json:"employee_id"`
	Date       string `json:"date"`
	StartTime  string `json:"start_time"`
	EndTime    string `json:"end_time"`
	Type       string `json:"type"`
}

type ConsultationUpdate struct {
	Id         string `json:"id"`
	PatientId  string `json:"patient_id"`
	EmployeeId string `json:"employee_id"`
	Date       string `json:"date"`
	StartTime  string `json:"start_time"`
	EndTime    string `json:"end_time"`
	Type       string `json:"type"`
}

type ConsultationPrimaryKey struct {
	Id string `json:"id"`
}

type ConsultationGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type ConsultationGetListResponse struct {
	Count        int             `json:"count"`
	Consultation []*Consultation `json:"consultation"`
}
