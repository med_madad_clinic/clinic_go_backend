package storage

import (
	"clinic/models"
	"context"
)

type StorageI interface {
	Close()
	Patient() PatientI
	Role() RoleI
	Employee() EmployeeI
	Procedure() ProcedureI
	Questionnaire() QuestionnaireI
	Salary() SalaryI
	Expense() ExpenseI
	Payment() PaymentI
	Category() CategoryI
	PatientDiagnosis() PatientDiagnosisI
	Diagnosis() DiagnosisI
	Schedule() ScheduleI
	FinishedProcedure() FinishedProcedureI
	Dashboard() DashboardI
	NursePatient() NursePatientI
	Consultation() ConsultationI
	Treatment() TreatmentI
	TreatmentProcedure() TreatmentProcedureI
}

type PatientI interface {
	Create(ctx context.Context, req *models.PatientCreate) (*models.Patient, error)
	GetByID(ctx context.Context, req *models.PatientPrimaryKey) (*models.Patient, error)
	GetList(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error)
	Update(ctx context.Context, req *models.PatientUpdate) (int64, error)
	Delete(ctx context.Context, req *models.PatientPrimaryKey) error
	GetListConsultationPatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error)
	GetListNursePatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error)
	GetListDeletedPatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error)
}

type EmployeeI interface {
	Create(ctx context.Context, req *models.EmployeeCreate) (*models.Employee, error)
	GetByID(ctx context.Context, req *models.EmployeePrimaryKey) (*models.Employee, error)
	GetList(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error)
	GetListNurse(ctx context.Context) (*models.EmployeeGetListResponse, error)
	Update(ctx context.Context, req *models.EmployeeUpdate) (int64, error)
	Delete(ctx context.Context, req *models.EmployeePrimaryKey) error
	GetByLogin(ctx context.Context, req *models.EmployeePrimaryKey) (*models.Employee, error)
	GetListDoctors(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error)
	UpdateEmlpoyeeBalance(ctx context.Context, req *models.EmployeePrimaryKey) (int64, error)
	GetListDeletedEmployee(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error) 
}

type RoleI interface {
	Create(ctx context.Context, req *models.RoleCreate) (*models.Role, error)
	GetByID(ctx context.Context, req *models.RolePrimaryKey) (*models.Role, error)
	GetList(ctx context.Context, req *models.RoleGetListRequest) (*models.RoleGetListResponse, error)
	Update(ctx context.Context, req *models.RoleUpdate) (int64, error)
	Delete(ctx context.Context, req *models.RolePrimaryKey) error
}

type PatientDiagnosisI interface {
	Create(ctx context.Context, req *models.PatientDiagnosisCreate) (*models.PatientDiagnosis, error)
	GetByID(ctx context.Context, req *models.PatientDiagnosisPrimaryKey) (*models.PatientDiagnosis, error)
	GetList(ctx context.Context, req *models.PatientDiagnosisGetListRequest) (*models.PatientDiagnosisGetListResponse, error)
	Delete(ctx context.Context, req *models.PatientDiagnosisPrimaryKey) error
	GetDiagnosisProceduresByDiagnosisID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.PatientDiagnosisGetListResponse, error)
}

type ProcedureI interface {
	Create(ctx context.Context, req *models.ProcedureCreate) (*models.Procedure, error)
	GetByID(ctx context.Context, req *models.ProcedurePrimaryKey) (*models.Procedure, error)
	GetList(ctx context.Context, req *models.ProcedureGetListRequest) (*models.ProcedureGetListResponse, error)
	Update(ctx context.Context, req *models.ProcedureUpdate) (int64, error)
	Delete(ctx context.Context, req *models.ProcedurePrimaryKey) error
	GetProcdureByCategory(ctx context.Context) (*models.GetProcdureByCategory, error)
}

type QuestionnaireI interface {
	Create(ctx context.Context, req *models.QuestionnaireCreate) (*models.Questionnaire, error)
	GetByID(ctx context.Context, req *models.QuestionnairePrimaryKey) (*models.Questionnaire, error)
	GetList(ctx context.Context, req *models.QuestionnaireGetListRequest) (*models.QuestionnaireGetListResponse, error)
	Update(ctx context.Context, req *models.QuestionnaireUpdate) (int64, error)
	Delete(ctx context.Context, req *models.QuestionnairePrimaryKey) error
}

type SalaryI interface {
	Create(ctx context.Context, req *models.SalaryCreate) (*models.Salary, error)
	GetByID(ctx context.Context, req *models.SalaryPrimaryKey) (*models.Salary, error)
	GetList(ctx context.Context, req *models.SalaryGetListRequest) (*models.SalaryGetListResponse, error)
	Delete(ctx context.Context, req *models.SalaryPrimaryKey) error
}

type ExpenseI interface {
	Create(ctx context.Context, req *models.ExpenseCreate) (*models.Expense, error)
	GetByID(ctx context.Context, req *models.ExpensePrimaryKey) (*models.Expense, error)
	GetList(ctx context.Context, req *models.ExpenseGetListRequest) (*models.ExpenseGetListResponse, error)
	Delete(ctx context.Context, req *models.ExpensePrimaryKey) error
}

type PaymentI interface {
	Create(ctx context.Context, req *models.PaymentCreate) (*models.Payment, error)
	GetByID(ctx context.Context, req *models.PaymentPrimaryKey) (*models.Payment, error)
	GetList(ctx context.Context, req *models.PaymentGetListRequest) (*models.PaymentGetListResponse, error)
	Delete(ctx context.Context, req *models.PaymentPrimaryKey) error
}

type CategoryI interface {
	Create(ctx context.Context, req *models.CategoryCreate) (*models.Category, error)
	GetByID(ctx context.Context, req *models.CategoryPrimaryKey) (*models.Category, error)
	GetList(ctx context.Context, req *models.CategoryGetListRequest) (*models.CategoryGetListResponse, error)
	Update(ctx context.Context, req *models.CategoryUpdate) (int64, error)
	Delete(ctx context.Context, req *models.CategoryPrimaryKey) error
}

type DiagnosisI interface {
	Create(ctx context.Context, req *models.DiagnosisCreate) (*models.Diagnosis, error)
	GetByID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.Diagnosis, error)
	GetList(ctx context.Context, req *models.DiagnosisGetListRequest) (*models.DiagnosisGetListResponse, error)
	Update(ctx context.Context, req *models.DiagnosisUpdate) (int64, error)
	Delete(ctx context.Context, req *models.DiagnosisPrimaryKey) error
}

type ScheduleI interface {
	Create(ctx context.Context, req *models.ScheduleCreate) (*models.Schedule, error)
	GetByID(ctx context.Context, req *models.SchedulePrimaryKey) (*models.Schedule, error)
	GetList(ctx context.Context, req *models.ScheduleGetListRequest) (*models.ScheduleGetListResponse, error)
	Delete(ctx context.Context, req *models.SchedulePrimaryKey) error
	GetEmployeesSchedule(ctx context.Context, req *models.GetEmployeesScheduleRequest) (*models.GetEmployeesScheduleResponse, error)
	GetNurseSchedule(ctx context.Context, req *models.GetEmployeesScheduleRequest) (*models.GetEmployeesScheduleResponse, error)
}

type FinishedProcedureI interface {
	Create(ctx context.Context, req *models.FinishedProceduresCreate) (*models.FinishedProcedures, error)
	GetByID(ctx context.Context, req *models.FinishedProceduresPrimaryKey) (*models.FinishedProcedures, error)
	GetList(ctx context.Context, req *models.FinishedProceduresGetListRequest) (*models.FinishedProceduresGetListResponse, error)
	Delete(ctx context.Context, req *models.FinishedProceduresPrimaryKey) error
}

type DashboardI interface {
	TotalCount(ctx context.Context) (*models.TotalCount, error)
	ScheduleToday(ctx context.Context, date string ) (*models.GetListTodayScheduleResponse, error)
	GetListConsultations(ctx context.Context ) (*models.ConsultationGetListResponse, error)
	PatientStatistics(ctx context.Context) (*models.PatientDashoard, error)
}

type NursePatientI interface {
	Create(ctx context.Context, req *models.NursePatientCreate) error
	Delete(ctx context.Context, req *models.DeleteNursePatient) error
	GetNursePatients(ctx context.Context, req *models.EmployeePrimaryKey) (*models.PatientGetListResponse, error)
}

type ConsultationI interface {
	Create(ctx context.Context, req *models.ConsultationCreate) (*models.Consultation, error)
	GetByID(ctx context.Context, req *models.ConsultationPrimaryKey) (*models.Consultation, error)
	GetList(ctx context.Context, req *models.ConsultationGetListRequest) (*models.ConsultationGetListResponse, error)
	Delete(ctx context.Context, req *models.ConsultationPrimaryKey) error
}

type TreatmentI interface {
	Create(ctx context.Context, req *models.TreatmentCreate) (*models.Treatment, error)
	GetByID(ctx context.Context, req *models.TreatmentPrimaryKey) (*models.Treatment, error)
	GetList(ctx context.Context, req *models.TreatmentGetListRequest) (*models.TreatmentGetListResponse, error)
	Delete(ctx context.Context, req *models.TreatmentPrimaryKey) error
	GetDiagnosisProceduresByDiagnosisID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.GetProcedureByDiagnosisId, error)
}

type TreatmentProcedureI interface {
	Create(ctx context.Context, req *models.TreatmentProcedureCreate) (*models.TreatmentProcedure, error)
	GetByID(ctx context.Context, req *models.TreatmentProcedurePrimaryKey) (*models.TreatmentProcedure, error)
	GetList(ctx context.Context, req *models.TreatmentProcedureGetListRequest) (*models.TreatmentProcedureGetListResponse, error)
	Delete(ctx context.Context, req *models.TreatmentProcedurePrimaryKey) error
}
