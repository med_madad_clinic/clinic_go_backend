package models

type TotalCount struct {
	Treatment int    `json:"treatment"`
	Patient   int    `json:"patient"`
	Doctor    int    `json:"doctor"`
	Employee  int    `json:"employee"`
	Chart     *Chart `json:"chart"`
}

type PatientDashoard struct {
	TotalCount int `json:"total_count"`
	NewPatient int `json:"new_patient"`
	Man        int `json:"man"`
	Woman      int `json:"woman"`
}

type PatientStatistics struct {
	Count int    `json:"count"`
	Month string `json:"month"`
}

type PatientEachMonth struct {
	Data []*PatientStatistics `json:"data"`
}

type Data struct {
	Label            string `json:"label"`
	NumberOfPatients int64  `json:"numberOfPatients"`
}

type Chart struct {
	Week  *Week  `json:"week"`
	Month *Month `json:"month"`
	Year  *Year  `json:"year"`
}

type Week struct {
	Data []*Data `json:"data"`
}

type Month struct {
	Data []*Data `json:"data"`
}

type Year struct {
	Data []*Data `json:"data"`
}
