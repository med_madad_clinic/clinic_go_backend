package models

type Payment struct {
	Id        string `json:"id" gorm:"primaryKey"`
	Patient_Id string `json:"patient_id,omitempty"`
	Cash 	  float64 `json:"cash,omitempty"`
	Card      float64 `json:"card,omitempty"`
	Comment   string `json:"comment,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
}

type PaymentCreate struct {
	Patient_Id string `json:"patient_id"`
	Cash      float64 `json:"cash"`
	Card      float64 `json:"card"`
	Comment   string `json:"comment"`
}


type PaymentPrimaryKey struct {
	Id string `json:"id"`
}


type PaymentGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}


type PaymentGetListResponse struct {
	Count     int         `json:"count"`
	Payment  []*Payment  `json:"payments"`
}





