package models

type Role struct {
	Id         string `json:"id,omitempty"`
	Role_name  string `json:"role_name,omitempty"`
	Created_at string `json:"created_at,omitempty"`
	Updated_at string `json:"updated_at,omitempty"`
	Order      int  `json:"order,omitempty"`
}

type RoleCreate struct {
	Role_name string `json:"role_name"`
}

type RoleUpdate struct {
	Id        string `json:"id"`
	Role_name string `json:"role_name"`
}

type RolePrimaryKey struct {
	Id string `json:"id"`
}

type RoleGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type RoleGetListResponse struct {
	Count int     `json:"count"`
	Role  []*Role `json:"roles"`
}
