package models


type Url struct {
	Url string `json:"url"`
}

type MultipleFileUploadResponse struct {
	Url []*Url `json:"url"`
}

