package models

type Procedure struct {
	Id           string  `json:"id,omitempty"`
	CategoryId   string  `json:"category_id,omitempty"`
	CategoryName string  `json:"category_name,omitempty"`
	Name         string  `json:"name,omitempty"`
	Description  string  `json:"description,omitempty"`
	NurseShare   float64 `json:"nurse_share,omitempty"`
	Created_at   string  `json:"created_at,omitempty"`
	Updated_at   string  `json:"updated_at,omitempty"`
	Order        int     `json:"order,omitempty"`
}

type ProcedureCreate struct {
	CategoryId  string  `json:"category_id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	NurseShare  float64 `json:"nurse_share"`
}

type ProcedureUpdate struct {
	Id          string  `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	NurseShare  float64 `json:"nurse_share"`
}

type ProcedurePrimaryKey struct {
	Id string `json:"id"`
}

type ProcedureGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type ProcedureGetListResponse struct {
	Count     int          `json:"count"`
	Procedure []*Procedure `json:"procedures"`
}


type GetProcdureByCategory struct {
	Data []*ProceduresByCategory `json:"data,omitempty"`
}

type ProceduresByCategory struct {
	CategoryName string `json:"category_name,omitempty"`
	Procedures []*Procedure `json:"procedures,omitempty"`
}




