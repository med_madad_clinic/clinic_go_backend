package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Salarys godoc
// @ID create_salarys
// @Router /clinic/api/v1/salary [POST]
// @Summary Create Salarys
// @Description Create Salarys
// @Tags Salary
// @Accept json
// @Salary json
// @Param Salary body models.SalaryCreate true "CreateSalaryRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateSalary(c *gin.Context) {
	var (
		salaryCreate models.SalaryCreate
	)

	err := c.ShouldBindJSON(&salaryCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Salary Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Salary().Create(c.Request.Context(), &salaryCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Salary.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	_,err = h.storage.Employee().UpdateEmlpoyeeBalance(c.Request.Context(), &models.EmployeePrimaryKey{
		Id: resp.Employee_id,
		Amount: resp.Card+resp.Cash - 2*(resp.Card+resp.Cash),
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error salary.Create.Employee.UpdateEmlpoyeeBalance")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}


	h.logger.Info("Create Salary Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Salarys godoc
// @ID get_by_id_salarys
// @Router /clinic/api/v1/salary/{id} [GET]
// @Summary Get By ID Salarys
// @Description Get By ID Salarys
// @Tags Salary
// @Accept json
// @Salary json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdSalary(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Salary().GetByID(c.Request.Context(), &models.SalaryPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Salary.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Salary Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Salarys godoc
// @ID get_list_salarys
// @Router /clinic/api/v1/salarys [GET]
// @Summary Get List Salarys
// @Description Get List Salarys
// @Tags Salary
// @Accept json
// @Salary json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListSalary(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSalary INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSalary INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Salary().GetList(c.Request.Context(), &models.SalaryGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Salary.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListSalary Response!")
	c.JSON(http.StatusOK, resp)
}



// Delete Salarys godoc
// @ID delete_salarys
// @Router /clinic/api/v1/salary/{id} [DELETE]
// @Summary Delete Salarys
// @Description Delete Salarys
// @Tags Salary
// @Accept json
// @Salary json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteSalary(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Salary().Delete(c.Request.Context(), &models.SalaryPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Salary.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Salary Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
