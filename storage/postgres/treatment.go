package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type treatmentRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewTreatmentRepo(db *pgxpool.Pool, log logger.LoggerI) *treatmentRepo {
	return &treatmentRepo{
		db:  db,
		log: log,
	}
}

func (u *treatmentRepo) Create(ctx context.Context, req *models.TreatmentCreate) (*models.Treatment, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO treatment (
			id,
			diagnosis_id,
			employee_id,
			blood_presure,
			count,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.DiagnosisId, req.EmployeeId, req.BloodPresure, 0, currentTime)
	if err != nil {
		u.log.Error("error is while creating treatment data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.TreatmentPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id treatment" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *treatmentRepo) GetByID(ctx context.Context, req *models.TreatmentPrimaryKey) (*models.Treatment, error) {
	var (
		query         string
		id            sql.NullString
		diagnosis_id  sql.NullString
		employee_id   sql.NullString
		blood_presure sql.NullString
		status        sql.NullString
		count         sql.NullInt64
		created_at    sql.NullString
		updated_at    sql.NullString
	)

	query = `
		SELECT 
			id,
			diagnosis_id,
			employee_id,
			blood_presure,
			status,
			count,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "treatment" 
		WHERE id = $1 OR diagnosis_id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&diagnosis_id,
		&employee_id,
		&blood_presure,
		&status,
		&count,
		&created_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Treatment{
		Id:           id.String,
		DiagnosisId:  diagnosis_id.String,
		EmployeeId:   employee_id.String,
		BloodPresure: blood_presure.String,
		Status:       status.String,
		Count:        int(count.Int64),
		CreatedAt:    created_at.String,
		UpdatedAt:    updated_at.String,
	}, nil
}

func (u *treatmentRepo) GetList(ctx context.Context, req *models.TreatmentGetListRequest) (*models.TreatmentGetListResponse, error) {
	var (
		resp   = &models.TreatmentGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY t.created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			t.id,
			t.diagnosis_id,
			t.employee_id,
			e.full_name,
			t.blood_presure,
			t.status,
			t.count,
			TO_CHAR(t.created_at, 'dd/mm/yyyy'), 
			TO_CHAR(t.updated_at, 'dd/mm/yyyy')
		FROM "treatment" AS t
		JOIN employees AS e ON t.employee_id = e.id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.DiagnosisId) > 0 {
		where = fmt.Sprintf(" WHERE t.diagnosis_id = '%s' ", req.DiagnosisId)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting treatment list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id            sql.NullString
			diagnosis_id  sql.NullString
			employee_id   sql.NullString
			blood_presure sql.NullString
			employee_name sql.NullString
			status        sql.NullString
			count         sql.NullInt64
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&diagnosis_id,
			&employee_id,
			&employee_name,
			&blood_presure,
			&status,
			&count,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Treatment = append(resp.Treatment, &models.Treatment{
			Id:           id.String,
			DiagnosisId:  diagnosis_id.String,
			EmployeeId:   employee_id.String,
			EmployeeName: employee_name.String,
			BloodPresure: blood_presure.String,
			Status:       status.String,
			Count:        int(count.Int64),
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}
	return resp, nil
}

// func (u *treatmentRepo) Update(ctx context.Context, req *models.TreatmentUpdate) (int64, error) {
// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			"treatment"
// 		SET
// 			url = :url,
// 		WHERE id = :id
// 	`

// 	timeZone, err := time.LoadLocation("Asia/Tashkent")
// 	if err != nil {
// 		u.log.Error("error is while loading tashkent timezone" + err.Error())
// 		return 0, err
// 	}

// 	currentTime := time.Now().In(timeZone)

// 	params = map[string]interface{}{
// 		"id":         req.Id,
// 		"url":        req.Url,
// 		"updated_at": currentTime,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	result, err := u.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		u.log.Error("error is while updating treatment data", logger.Error(err))
// 		return 0, err
// 	}

// 	return result.RowsAffected(), nil
// }

func (u *treatmentRepo) Delete(ctx context.Context, req *models.TreatmentPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM "treatment" WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting treatments", logger.Error(err))
		return err
	}

	return nil
}

func (u *treatmentRepo) GetDiagnosisProceduresByDiagnosisID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.GetProcedureByDiagnosisId, error) {
	var (
		resp  = &models.GetProcedureByDiagnosisId{}
		query string
	)

	query = `
		SELECT
			c.procedure_id,
			name
		FROM patient_procedures AS c
		JOIN procedures AS p ON p.id = c.procedure_id
		WHERE diagnosis_id = $1

	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting patientDiagnosis list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			procedure_id sql.NullString
			name         sql.NullString
		)
		err = rows.Scan(
			&procedure_id,
			&name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.Procedures = append(resp.Procedures, &models.Procedure{
			Id:   procedure_id.String,
			Name: name.String,
		})
	}
	return resp, nil

}
