package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type scheduleRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewScheduleRepo(db *pgxpool.Pool, log logger.LoggerI) *scheduleRepo {
	return &scheduleRepo{
		db:  db,
		log: log,
	}
}

func (u *scheduleRepo) Create(ctx context.Context, req *models.ScheduleCreate) (*models.Schedule, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO schedule (
			id,
			employee_id,
			patient_id,
			day,
			start_date,
			start_time,
			end_time,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Employee_id, req.Patient_id, req.Day, req.Date, req.Start_time, req.End_time, currentTime)
	if err != nil {
		u.log.Error("error is while creating schedule data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id schedule" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *scheduleRepo) GetByID(ctx context.Context, req *models.SchedulePrimaryKey) (*models.Schedule, error) {
	var (
		query       string
		id          sql.NullString
		employee_id sql.NullString
		day         sql.NullString
		start_time  sql.NullString
		end_time    sql.NullString
		created_at  sql.NullString
		status      sql.NullString
	)

	query = `
		SELECT 
			id,
			employee_id,
			day,
			start_time,
			end_time,
			status,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "schedule" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&employee_id,
		&day,
		&start_time,
		&end_time,
		&status,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Schedule{
		Id:          id.String,
		Employee_id: employee_id.String,
		Day:         day.String,
		Start_time:  start_time.String,
		End_time:    end_time.String,
		Status:      status.String,
		Created_at:  created_at.String,
	}, nil
}

func (u *scheduleRepo) GetList(ctx context.Context, req *models.ScheduleGetListRequest) (*models.ScheduleGetListResponse, error) {
	var (
		resp   = &models.ScheduleGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			employee_id,
			day,
			start_time,
			end_time,
			status,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
		FROM "schedule" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting schedule list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			employee_id sql.NullString
			day         sql.NullString
			start_time  sql.NullString
			end_time    sql.NullString
			created_at  sql.NullString
			status      sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&employee_id,
			&day,
			&start_time,
			&end_time,
			&status,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Schedule = append(resp.Schedule, &models.Schedule{
			Id:          id.String,
			Employee_id: employee_id.String,
			Day:         day.String,
			Start_time:  start_time.String,
			End_time:    end_time.String,
			Status:      status.String,
			Created_at:  created_at.String,
		})
	}
	return resp, nil
}

// func (u *scheduleRepo) Update(ctx context.Context, req *models.ScheduleUpdate) (int64, error) {
// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			"schedule"
// 		SET
// 			full_name = :full_name,
// 			phone = :phone,
// 			birthday = :birthday,
// 			update = :updated_at
// 		WHERE id = :id
// 	`

// 	timeZone, err := time.LoadLocation("Asia/Tashkent")
// 	if err != nil {
// 		u.log.Error("error is while loading tashkent timezone" + err.Error())
// 		return 0, err
// 	}

// 	currentTime := time.Now().In(timeZone)

// 	params = map[string]interface{}{
// 		"id":         req.Id,
// 		"full_name":  req.Full_name,
// 		"birthday":   req.Birthday,
// 		"phone":      req.Phone,
// 		"updated_at": currentTime,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	result, err := u.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		u.log.Error("error is while updating schedule data", logger.Error(err))
// 		return 0, err
// 	}

// 	return result.RowsAffected(), nil
// }

func (u *scheduleRepo) Delete(ctx context.Context, req *models.SchedulePrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `DELETE FROM schedule WHERE id = $1`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting schedule", logger.Error(err))
		return err
	}

	return nil
}

func (u *scheduleRepo) GetEmployeesSchedule(ctx context.Context, req *models.GetEmployeesScheduleRequest) (*models.GetEmployeesScheduleResponse, error) {
	date, err := time.Parse("2006-01-02", req.Date)
	if err != nil {
		return nil, err
	}

	day := date.Weekday().String()

	var (
		query string
		resp  = &models.GetEmployeesScheduleResponse{}
	)

	query = `
		SELECT 
			distinct(s.employee_id),
			e.full_name
		FROM "schedule" AS s
		JOIN "employees" AS e ON s.employee_id = e.id
		WHERE s.day =$1 AND s.start_date <= $2
	`

	query1 := `
		SELECT 
			s.patient_id,
			p.full_name,
			p.phone,
			p.address,
			s.day,
			TO_CHAR(s.start_time, 'HH24:MI'),
			TO_CHAR(s.end_time, 'HH24:MI')
		FROM "schedule" AS s
		JOIN "patients" AS p ON s.patient_id = p.id
		WHERE s.day =$1 AND s.start_date <= $2 AND s.employee_id = $3

	
	
	
	`

	rows, err := u.db.Query(ctx, query, day, date)
	if err != nil {
		u.log.Error("error is while getting user list", logger.Error(err))
		return nil, err
	}
	for rows.Next() {
		var (
			patient       = &models.PatientGetListResponse{}
			schedule      = &models.EmployeeShedule{}
			employee_id   sql.NullString
			patient_id    sql.NullString
			employee_name sql.NullString
			patient_name  sql.NullString
			phone         sql.NullString
			address       sql.NullString
			day           sql.NullString
			start_time    sql.NullString
			end_time      sql.NullString
		)

		err = rows.Scan(
			&employee_id,
			&employee_name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		rows1, err := u.db.Query(ctx, query1, date.Weekday().String(), date.Format("2006-01-02"), employee_id.String)
		if err != nil {
			u.log.Error("error is while getting user list", logger.Error(err))
			return nil, err
		}

		for rows1.Next() {
			err = rows1.Scan(
				&patient_id,
				&patient_name,
				&phone,
				&address,
				&day,
				&start_time,
				&end_time,
			)
			if err != nil {
				u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
				return nil, err
			}
			patient.Patient = append(patient.Patient, &models.Patient{
				Id:         patient_id.String,
				Full_name:  patient_name.String,
				Phone:      phone.String,
				Address:    address.String,
				Start_time: start_time.String,
				End_time:   end_time.String,
			})

		}
		schedule.EmployeeId = employee_id.String
		schedule.EmployeeName = employee_name.String
		schedule.Patient = append(schedule.Patient, patient.Patient...)
		resp.Schedule = append(resp.Schedule, schedule)

		patient.Patient = nil

	}

	return resp, nil
}

func (u *scheduleRepo) GetNurseSchedule(ctx context.Context, req *models.GetEmployeesScheduleRequest) (*models.GetEmployeesScheduleResponse, error) {
	date, err := time.Parse("2006-01-02", req.Date)
	if err != nil {
		return nil, err
	}

	day := date.Weekday().String()

	var (
		query string
		resp  = &models.GetEmployeesScheduleResponse{}
	)

	query = `
		SELECT 
			s.employee_id,
			p.id,
			p.age,
			p.latitude,
			p.longitude,
			e.full_name,
			p.full_name,
			p.phone,
			p.address,
			s.day,
			s.start_time,
			s.end_time
		FROM "schedule" AS s
		JOIN "employees" AS e ON s.employee_id = e.id
		JOIN "patients" AS p ON s.patient_id = p.id
		WHERE s.day =$1 AND s.start_date <= $2 AND s.employee_id = $3
		ORDER BY s.start_time ASC
	`

	rows, err := u.db.Query(ctx, query, day, date, req.EmployeeId)
	if err != nil {
		u.log.Error("error is while getting user list", logger.Error(err))
		return nil, err
	}
	for rows.Next() {
		var (
			schedule      = &models.EmployeeShedule{}
			employee_id   sql.NullString
			patient_id    sql.NullString
			age           sql.NullInt16
			latitude      sql.NullString
			longitude     sql.NullString
			employee_name sql.NullString
			patient_name  sql.NullString
			phone         sql.NullString
			address       sql.NullString
			day           sql.NullString
			start_time    sql.NullString
			end_time      sql.NullString
		)

		err = rows.Scan(
			&employee_id,
			&patient_id,
			&age,
			&latitude,
			&longitude,
			&employee_name,
			&patient_name,
			&phone,
			&address,
			&day,
			&start_time,
			&end_time,
		)

		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		schedule.EmployeeId = employee_id.String
		schedule.EmployeeName = employee_name.String

		schedule.Patient = append(schedule.Patient, &models.Patient{
			Id:         patient_id.String,
			Full_name:  patient_name.String,
			Age:        int(age.Int16),
			Latitude:   latitude.String,
			Longitude:  longitude.String,
			Phone:      phone.String,
			Address:    address.String,
			Start_time: start_time.String,
			End_time:   end_time.String,
		})

		resp.Schedule = append(resp.Schedule, schedule)

	}

	return resp, nil
}
