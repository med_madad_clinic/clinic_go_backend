package api

import (
	"clinic/config"
	"clinic/pkg/logger"

	"clinic/storage"

	_ "clinic/api/docs"
	"clinic/api/handler"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewApi(r *gin.Engine, cfg *config.Config, storage storage.StorageI, logger logger.LoggerI) {

	h := handler.NewHandler(cfg, storage, logger)
	r.Use(customCORSMiddleware())
	v1 := r.Group("/clinic/api/v1")

	v1.POST("/patient", h.CreatePatient)
	v1.GET("/patient/:id", h.GetByIdPatient)
	v1.GET("/patients", h.GetListPatient)
	v1.GET("consultation-patients", h.GetListConsultationPatient)
	v1.GET("nurse-patients", h.GetListNursePatient)
	v1.GET("deleted-patients", h.GetListDeletedPatient)
	v1.PUT("/patient/:id", h.UpdatePatient)
	v1.DELETE("/patient/:id", h.DeletePatient)

	v1.POST("/procedure", h.CreateProcedure)
	v1.GET("/procedure/:id", h.GetByIdProcedure)
	v1.GET("/procedures", h.GetListProcedure)
	v1.PUT("/procedure/:id", h.UpdateProcedure)
	v1.DELETE("/procedure/:id", h.DeleteProcedure)
	v1.GET("proceduresbycategory", h.GetListProcedureByCategory)

	v1.POST("/salary", h.CreateSalary)
	v1.GET("/salary/:id", h.GetByIdSalary)
	v1.GET("/salaries", h.GetListSalary)
	v1.DELETE("/salary/:id", h.DeleteSalary)

	v1.POST("/role", h.CreateRole)
	v1.GET("/role/:id", h.GetByIdRole)
	v1.GET("/roles", h.GetListRole)
	v1.PUT("/role/:id", h.UpdateRole)
	v1.DELETE("/role/:id", h.DeleteRole)

	v1.POST("employee", h.CreateEmployee)
	v1.GET("employee/:id", h.GetByIdEmployee)
	v1.GET("employees", h.GetListEmployee)
	v1.GET("deleted-employees", h.GetListDeletedEmployee)
	v1.PUT("employee/:id", h.UpdateEmployee)
	v1.DELETE("employee/:id", h.DeleteEmployee)
	v1.GET("doctors", h.GetListDoctors)
	v1.GET("all-nurse", h.GetListNurse)

	v1.POST("questionnaire", h.CreateQuestionnaire)
	v1.GET("questionnaire/:id", h.GetByIdQuestionnaire)
	v1.GET("questionnaires", h.GetListQuestionnaire)
	v1.PUT("questionnaire/:id", h.UpdateQuestionnaire)
	v1.DELETE("questionnaire/:id", h.DeleteQuestionnaire)

	v1.POST("expense", h.CreateExpense)
	v1.GET("expense/:id", h.GetByIdExpense)
	v1.GET("expenses", h.GetListExpense)
	v1.DELETE("expense/:id", h.DeleteExpense)

	v1.POST("payment", h.CreatePayment)
	v1.GET("payment/:id", h.GetByIdPayment)
	v1.GET("payments", h.GetListPayment)
	v1.DELETE("payment/:id", h.DeletePayment)

	v1.POST("patient-diagnosis", h.CreatePatientDiagnosis)
	v1.GET("patient-diagnosis/:id", h.GetByIDPatientDiagnosis)
	v1.GET("patient-diagnosises", h.GetListPatientDiagnosis)
	// v1.PUT("patient-diagnosis/:id", h.UpdatePatientDiagnosis)
	v1.DELETE("patient-diagnosis/:id", h.DeletePatientDiagnosis)

	v1.POST("schedule", h.CreateSchedule)
	v1.GET("schedule/:id", h.GetByIdSchedule)
	v1.GET("schedules", h.GetListSchedule)
	v1.GET("employee-schedule", h.GetListEmlpoyeeSchedule)
	v1.GET("nurse-schedule", h.GetListNurseSchedule)
	v1.DELETE("schedule/:id", h.DeleteSchedule)

	v1.POST("category", h.CreateCategory)
	v1.GET("category/:id", h.GetByIdCategory)
	v1.GET("categories", h.GetListCategory)
	v1.PUT("category/:id", h.UpdateCategory)
	v1.DELETE("category/:id", h.DeleteCategory)

	v1.POST("diagnosis", h.CreateDiagnosis)
	v1.GET("diagnosis/:id", h.GetByIdDiagnosis)
	v1.GET("diagnosiss", h.GetListDiagnosis)
	v1.PUT("diagnosis/:id", h.UpdateDiagnosis)
	v1.DELETE("diagnosis/:id", h.DeleteDiagnosis)

	v1.POST("nursepatients", h.CreateNursePatient)
	v1.DELETE("nursepatients", h.DeleteNursePatient)
	v1.GET("getlistnursepatients", h.GetListNursePatients)

	v1.POST("consultation", h.CreateConsultation)
	v1.GET("consultation/:id", h.GetByIdConsultation)
	v1.GET("consultation", h.GetListConsultation)
	v1.DELETE("consultation/:id", h.DeleteConsultation)

	v1.POST("treatment", h.CreateTreatment)
	v1.GET("treatment/:id", h.GetByIdTreatment)
	v1.GET("treatments", h.GetListTreatment)
	v1.DELETE("treatment/:id", h.DeleteTreatment)
	v1.GET("treatment-procedures", h.GetTreatmentProcedures)
	v1.GET("diagnosis-procedures", h.GetProcedureByDiagnosisId)

	v1.POST("treatmentprocedure", h.CreateTreatmentProcedure)
	v1.GET("treatmentprocedure/:id", h.GetByIdTreatmentProcedure)
	v1.GET("treatmentprocedure", h.GetListTreatmentProcedure)
	v1.DELETE("treatmentprocedure/:id", h.DeleteTreatmentProcedure)

	v1.GET("dashboard", h.AdminDashboard)
	v1.GET("today-schedule", h.TodaySchedule)
	v1.GET("consultations-list", h.ConsultationsList)
	v1.GET("patient-statistics", h.PatientsStatistics)

	v1.POST("auth/login", h.Login)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
