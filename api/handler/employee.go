package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Employees godoc
// @ID create_employees
// @Router /clinic/api/v1/employee [POST]
// @Summary Create Employees
// @Description Create Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param Employee body models.EmployeeCreate true "CreateEmployeeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateEmployee(c *gin.Context) {
	var (
		employeeCreate models.EmployeeCreate
	)

	err := c.ShouldBindJSON(&employeeCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Employee Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Employee().Create(c.Request.Context(), &employeeCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Employee.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Employee Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Employees godoc
// @ID get_by_id_employees
// @Router /clinic/api/v1/employee/{id} [GET]
// @Summary Get By ID Employees
// @Description Get By ID Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdEmployee(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Employee().GetByID(c.Request.Context(), &models.EmployeePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Employee Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Employees godoc
// @ID get_list_employees
// @Router /clinic/api/v1/employees [GET]
// @Summary Get List Employees
// @Description Get List Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Param role_id query string false "role_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListEmployee(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListEmployee INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListEmployee INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Employee().GetList(c.Request.Context(), &models.EmployeeGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
		Role: c.Query("role_id"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListEmployee Response!")
	c.JSON(http.StatusOK, resp)
}


// GetList Deleted Employees godoc
// @ID get_list_Deleted employees
// @Router /clinic/api/v1/deleted-employees [GET]
// @Summary Get List Deleted Employees
// @Description Get List Deleted Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Param role_id query string false "role_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListDeletedEmployee(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListEmployee INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListEmployee INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Employee().GetListDeletedEmployee(c.Request.Context(), &models.EmployeeGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
		Role: c.Query("role_id"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListEmployee Response!")
	c.JSON(http.StatusOK, resp)
}


// GetList doctors godoc
// @ID get_list_doctors
// @Router /clinic/api/v1/doctors [GET]
// @Summary Get List doctors
// @Description Get List doctors
// @Tags Employee
// @Accept json
// @Employee json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListDoctors(c *gin.Context) {

	

	resp, err := h.storage.Employee().GetListDoctors(c.Request.Context(), &models.EmployeeGetListRequest{
		
	})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListEmployee Response!")
	c.JSON(http.StatusOK, resp)
}




// Update Employees godoc
// @ID update_employees
// @Router /clinic/api/v1/employee/{id} [PUT]
// @Summary Update Employees
// @Description Update Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param id path string true "id"
// @Param Employee body models.EmployeeUpdate true "UpdateEmployeeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateEmployee(c *gin.Context) {
	var (
		id             = c.Param("id")
		employeeUpdate models.EmployeeUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&employeeUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Employee Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	employeeUpdate.Id = id
	rowsAffected, err := h.storage.Employee().Update(c.Request.Context(), &employeeUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Employee.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Employee().GetByID(c.Request.Context(), &models.EmployeePrimaryKey{Id: employeeUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Employee Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Employees godoc
// @ID delete_employees
// @Router /clinic/api/v1/employee/{id} [DELETE]
// @Summary Delete Employees
// @Description Delete Employees
// @Tags Employee
// @Accept json
// @Employee json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteEmployee(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Employee().Delete(c.Request.Context(), &models.EmployeePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Employee Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}


// GetList nurse godoc
// @ID get_list_nurses
// @Router /clinic/api/v1/all-nurse [GET]
// @Summary Get List nurse
// @Description Get List nurse
// @Tags Employee
// @Accept json
// @Employee json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListNurse(c *gin.Context) {
	resp, err := h.storage.Employee().GetListNurse(c.Request.Context())
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Employee.GetListNurse!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	c.JSON(200,resp)
}




