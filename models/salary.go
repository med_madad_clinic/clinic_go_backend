package models

type Salary struct {
	Id          string  `json:"id,omitempty"`
	Employee_id string  `json:"employee_id,omitempty"`
	Cash        float64 `json:"cash,omitempty"`
	Card        float64 `json:"card,omitempty"`
	Comment     string  `json:"comment,omitempty"`
	Created_at  string  `json:"created_at,omitempty"`
}

type SalaryCreate struct {
	Employee_id string  `json:"employee_id,omitempty"`
	Cash        float64 `json:"cash,omitempty"`
	Card        float64 `json:"card,omitempty"`
	Comment     string  `json:"comment,omitempty"`
}

type SalaryPrimaryKey struct {
	Id string `json:"id"`
}

type SalaryGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type SalaryGetListResponse struct {
	Count  int       `json:"count"`
	Salary []*Salary `json:"salary"`
}
