package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type expenseRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewExpenseRepo(db *pgxpool.Pool, log logger.LoggerI) *expenseRepo {
	return &expenseRepo{
		db:  db,
		log: log,
	}
}

func (u *expenseRepo) Create(ctx context.Context, req *models.ExpenseCreate) (*models.Expense, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO expenses (
			id,
			employee_id,
			name,
			cash,
			card,
			comment,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Employee_id, req.Name, req.Cash, req.Card, req.Comment, currentTime)
	if err != nil {
		u.log.Error("error is while creating expense data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ExpensePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id expense" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *expenseRepo) GetByID(ctx context.Context, req *models.ExpensePrimaryKey) (*models.Expense, error) {
	var (
		query       string
		id          sql.NullString
		employee_id sql.NullString
		name        sql.NullString
		cash        sql.NullFloat64
		card        sql.NullFloat64
		comment     sql.NullString
		created_at  sql.NullString
	)

	query = `
		SELECT 
			id,
			employee_id,
			name,
			cash,
			card,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy') 
		FROM "expenses"
		WHERE deleted_at is null AND id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&employee_id,
		&name,
		&cash,
		&card,
		&comment,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Expense{
		Id:          id.String,
		Employee_id: employee_id.String,
		Name:        name.String,
		Cash:        cash.Float64,
		Card:        card.Float64,
		Comment:     comment.String,
		Created_at:  created_at.String,
	}, nil
}

func (u *expenseRepo) GetList(ctx context.Context, req *models.ExpenseGetListRequest) (*models.ExpenseGetListResponse, error) {
	var (
		resp   = &models.ExpenseGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY ex.created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			ex.id,
			ex.employee_id,
			emp.full_name,
			ex.name,
			ex.cash,
			ex.card,
			ex.comment,
			TO_CHAR(ex.created_at,'dd/mm/yyyy')
		FROM "expenses" AS ex
		JOIN employees AS emp ON ex.employee_id = emp.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting expense list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			employee_id sql.NullString
			employee    sql.NullString
			name        sql.NullString
			cash        sql.NullFloat64
			card        sql.NullFloat64
			comment     sql.NullString
			created_at  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&employee_id,
			&employee,
			&name,
			&cash,
			&card,
			&comment,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Expense = append(resp.Expense, &models.Expense{
			Id:          id.String,
			Employee_id: employee_id.String,
			Employee:    employee.String,
			Name:        name.String,
			Cash:        cash.Float64,
			Card:        card.Float64,
			Comment:     comment.String,
			Created_at:  created_at.String,
		})
	}
	return resp, nil
}

func (u *expenseRepo) Delete(ctx context.Context, req *models.ExpensePrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE expenses SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting expenses", logger.Error(err))
		return err
	}

	return nil
}
