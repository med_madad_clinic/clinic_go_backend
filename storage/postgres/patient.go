package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type patientRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewPatientRepo(db *pgxpool.Pool, log logger.LoggerI) *patientRepo {
	return &patientRepo{
		db:  db,
		log: log,
	}
}

func (u *patientRepo) Create(ctx context.Context, req *models.PatientCreate) (*models.Patient, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO patients (
			id,
			full_name,
			birthday,
			gender,
			age,
			height,
			weight,
			phone,
			home_phone,
			address,
			apartment_number,
			latitude,
			longitude,
			enterance,
			floor,
			home_number,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	parts := strings.Split(req.Latitude, ",")
	if len(parts) != 2 {
		u.log.Error("error is while creating patient data latitude")
		return nil, errors.New("error is while creating patient data latitude")
	}

	// Trim any whitespace and convert to float64
	latStr := strings.TrimSpace(parts[0])
	lonStr := strings.TrimSpace(parts[1])

	_, err = u.db.Exec(ctx, query, id, req.Full_name, req.Birthday, req.Gender, req.Age, req.Height, req.Weight, req.Phone, req.HomePhone, req.Address, req.Apartment_number, latStr, lonStr, req.Enterance, req.Floor, req.Home_number, currentTime)
	if err != nil {
		u.log.Error("error is while creating patient data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.PatientPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id patient" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *patientRepo) GetByID(ctx context.Context, req *models.PatientPrimaryKey) (*models.Patient, error) {
	var (
		query            string
		id               sql.NullString
		full_name        sql.NullString
		birthday         sql.NullString
		gender           sql.NullString
		age              sql.NullInt64
		height           sql.NullString
		weight           sql.NullString
		phone            sql.NullString
		home_phone       sql.NullString
		address          sql.NullString
		apartment_number sql.NullString
		latitude         sql.NullString
		longitude        sql.NullString
		enterance        sql.NullString
		floor            sql.NullString
		home_number      sql.NullString
		created_at       sql.NullString
		updated_at       sql.NullString
	)

	query = `
		SELECT 
			id,
			full_name,
			TO_CHAR(birthday,'yyyy-mm-dd'),
			gender,
			age,
			height,
			weight,
			phone,
			home_phone,
			address,
			apartment_number,
			latitude,
			longitude,
			enterance,
			floor,
			home_number,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "patients" 
		WHERE deleted_at is null AND id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&full_name,
		&latitude,
		&longitude,
		&age,
		&birthday,
		&gender,
		&height,
		&weight,
		&phone,
		&home_phone,
		&address,
		&apartment_number,
		&enterance,
		&floor,
		&home_number,
		&created_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Patient{
		Id:               id.String,
		Full_name:        full_name.String,
		Latitude:         latitude.String,
		Longitude:        longitude.String,
		Age:              int(age.Int64),
		Birthday:         birthday.String,
		Gender:           gender.String,
		Height:           height.String,
		Weight:           weight.String,
		Phone:            phone.String,
		HomePhone:        home_phone.String,
		Address:          address.String,
		Apartment_number: apartment_number.String,
		Enterance:        enterance.String,
		Floor:            floor.String,
		Home_number:      home_number.String,
		Created_at:       created_at.String,
		Updated_at:       updated_at.String,
	}, nil
}

func (u *patientRepo) GetList(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error) {
	var (
		resp   = &models.PatientGetListResponse{}
		query  string
		where  = " WHERE TRUE AND deleted_at is null "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			full_name,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			gender,
			age,
			height,
			weight,
			phone,
			home_phone,
			address,
			apartment_number,
			latitude,
			longitude,
			enterance,
			floor,
			home_number,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "patients" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting patient list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id               sql.NullString
			full_name        sql.NullString
			birthday         sql.NullString
			gender           sql.NullString
			age              sql.NullInt64
			height           sql.NullString
			weight           sql.NullString
			phone            sql.NullString
			home_phone       sql.NullString
			address          sql.NullString
			apartment_number sql.NullString
			latitude         sql.NullString
			longitude        sql.NullString
			enterance        sql.NullString
			floor            sql.NullString
			home_number      sql.NullString
			created_at       sql.NullString
			updated_at       sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
			&birthday,
			&gender,
			&age,
			&height,
			&weight,
			&phone,
			&home_phone,
			&address,
			&apartment_number,
			&latitude,
			&longitude,
			&enterance,
			&floor,
			&home_number,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Patient = append(resp.Patient, &models.Patient{
			Id:               id.String,
			Full_name:        full_name.String,
			Latitude:         latitude.String,
			Longitude:        longitude.String,
			Age:              int(age.Int64),
			Birthday:         birthday.String,
			Gender:           gender.String,
			Height:           height.String,
			Weight:           weight.String,
			Phone:            phone.String,
			HomePhone:        home_phone.String,
			Address:          address.String,
			Apartment_number: apartment_number.String,
			Enterance:        enterance.String,
			Floor:            floor.String,
			Home_number:      home_number.String,
			Created_at:       created_at.String,
			Updated_at:       updated_at.String,
		})
	}
	return resp, nil
}

func (u *patientRepo) GetListDeletedPatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error) {
	var (
		resp   = &models.PatientGetListResponse{}
		query  string
		where  = " WHERE TRUE AND deleted_at IS NOT NULL"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			full_name,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			gender,
			age,
			height,
			weight,
			phone,
			home_phone,
			address,
			apartment_number,
			latitude,
			longitude,
			enterance,
			floor,
			home_number,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy'),
			TO_CHAR(deleted_at,'dd/mm/yyyy')
		FROM "patients" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting patient list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id               sql.NullString
			full_name        sql.NullString
			birthday         sql.NullString
			gender           sql.NullString
			age              sql.NullInt64
			height           sql.NullString
			weight           sql.NullString
			phone            sql.NullString
			home_phone       sql.NullString
			address          sql.NullString
			apartment_number sql.NullString
			latitude         sql.NullString
			longitude        sql.NullString
			enterance        sql.NullString
			floor            sql.NullString
			home_number      sql.NullString
			created_at       sql.NullString
			updated_at       sql.NullString
			deleted_at       sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
			&birthday,
			&gender,
			&age,
			&height,
			&weight,
			&phone,
			&home_phone,
			&address,
			&apartment_number,
			&latitude,
			&longitude,
			&enterance,
			&floor,
			&home_number,
			&created_at,
			&updated_at,
			&deleted_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Patient = append(resp.Patient, &models.Patient{
			Id:               id.String,
			Full_name:        full_name.String,
			Latitude:         latitude.String,
			Longitude:        longitude.String,
			Age:              int(age.Int64),
			Birthday:         birthday.String,
			Gender:           gender.String,
			Height:           height.String,
			Weight:           weight.String,
			Phone:            phone.String,
			HomePhone:        home_phone.String,
			Address:          address.String,
			Apartment_number: apartment_number.String,
			Enterance:        enterance.String,
			Floor:            floor.String,
			Home_number:      home_number.String,
			Created_at:       created_at.String,
			Updated_at:       updated_at.String,
			Deleted_at:       deleted_at.String,
			Order:            int64(order),
		})
		order++
	}
	return resp, nil
}

func (u *patientRepo) Update(ctx context.Context, req *models.PatientUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"patients"
		SET
			full_name =:full_name,
			latitude =:latitude,
			longitude =:longitude,
			birthday =:birthday,
			gender =:gender,
			height =:height,
			weight =:weight,
			phone =:phone,
			home_phone =:home_phone,
			address =:address,
			apartment_number =:apartment_number,
			enterance =:enterance,
			floor =:floor,
			home_number =:home_number,
			updated_at =:updated_at
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":               req.Id,
		"full_name":        req.Full_name,
		"latitude":         req.Latitude,
		"longitude":        req.Longitude,
		"birthday":         req.Birthday,
		"gender":           req.Gender,
		"height":           req.Height,
		"weight":           req.Weight,
		"phone":            req.Phone,
		"home_phone":       req.HomePhone,
		"address":          req.Address,
		"apartment_number": req.Apartment_number,
		"enterance":        req.Enterance,
		"floor":            req.Floor,
		"home_number":      req.Home_number,
		"updated_at":       currentTime,
	}
	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating patient data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *patientRepo) Delete(ctx context.Context, req *models.PatientPrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE patients SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting patients", logger.Error(err))
		return err
	}

	return nil
}

func (u *patientRepo) GetListPatientDiagnosis(ctx context.Context, req *models.PatientPrimaryKey) (*models.DiagnosisGetListResponse, error) {
	var (
		query       string
		resp        = &models.DiagnosisGetListResponse{}
		id          sql.NullString
		employee_id sql.NullString
		patient_id  sql.NullString
		comment     sql.NullString
		count       sql.NullInt64
		created_at  sql.NullString
	)

	query = `
		SELECT
			COUNT(*) OVER()
			id,
			employee_id,
			patient_id,
			comment,
			count,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "diagnosis"
		WHERE patient_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting diagnosis list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {

		err := rows.Scan(
			&resp.Count,
			&id,
			&employee_id,
			&patient_id,
			&comment,
			&count,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.Diagnosis = append(resp.Diagnosis, &models.Diagnosis{
			Id:         id.String,
			EmployeeID: employee_id.String,
			PatientId:  patient_id.String,
			Comment:    comment.String,
			Count:      count.Int64,
			Created_at: created_at.String,
		})
	}

	return nil, nil
}

func (u *patientRepo) GetListConsultationPatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error) {
	var (
		query  string
		resp   = &models.PatientGetListResponse{}
		where  = " WHERE TRUE AND c.status ='new' AND p.deleted_at IS NULL"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY p.created_at DESC"
	)
	fmt.Println("ok")

	query =
		`
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.full_name,
			e.full_name,
			TO_CHAR(p.birthday, 'yyyy-mm-dd'),
			p.gender,
			p.age,
			p.height,
			p.weight,
			p.phone,
			p.home_phone,
			p.address,
			p.apartment_number,
			p.latitude,
			p.longitude,
			p.enterance,
			p.floor,
			p.home_number,
			TO_CHAR(p.created_at,'dd/mm/yyyy'), 
			TO_CHAR(p.updated_at,'dd/mm/yyyy')
		FROM consultations AS c
		JOIN patients as p ON c.patient_id = p.id
		JOIN employees as e ON c.employee_id = e.id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting patient list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id               sql.NullString
			full_name        sql.NullString
			doctor           sql.NullString
			birthday         sql.NullString
			gender           sql.NullString
			age              sql.NullInt64
			height           sql.NullString
			weight           sql.NullString
			phone            sql.NullString
			home_phone       sql.NullString
			address          sql.NullString
			apartment_number sql.NullString
			latitude         sql.NullString
			longitude        sql.NullString
			enterance        sql.NullString
			floor            sql.NullString
			home_number      sql.NullString
			created_at       sql.NullString
			updated_at       sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
			&doctor,
			&birthday,
			&gender,
			&age,
			&height,
			&weight,
			&phone,
			&home_phone,
			&address,
			&apartment_number,
			&latitude,
			&longitude,
			&enterance,
			&floor,
			&home_number,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Patient = append(resp.Patient, &models.Patient{
			Id:               id.String,
			Full_name:        full_name.String,
			Doctor:           doctor.String,
			Latitude:         latitude.String,
			Longitude:        longitude.String,
			Age:              int(age.Int64),
			Birthday:         birthday.String,
			Gender:           gender.String,
			Height:           height.String,
			Weight:           weight.String,
			Phone:            phone.String,
			HomePhone:        home_phone.String,
			Address:          address.String,
			Apartment_number: apartment_number.String,
			Enterance:        enterance.String,
			Floor:            floor.String,
			Home_number:      home_number.String,
			Created_at:       created_at.String,
			Updated_at:       updated_at.String,
		})
	}

	return resp, nil

}

func (u *patientRepo) GetListNursePatients(ctx context.Context, req *models.PatientGetListRequest) (*models.PatientGetListResponse, error) {
	var (
		query  string
		resp   = &models.PatientGetListResponse{}
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY p.created_at DESC"
	)

	query =
		`
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.full_name,
			e.full_name,
			TO_CHAR(p.birthday, 'yyyy-mm-dd'),
			p.gender,
			p.age,
			p.height,
			p.weight,
			p.phone,
			p.home_phone,
			p.address,
			p.apartment_number,
			p.latitude,
			p.longitude,
			p.enterance,
			p.floor,
			p.home_number,
			TO_CHAR(p.created_at,'dd/mm/yyyy'), 
			TO_CHAR(p.updated_at,'dd/mm/yyyy')
		FROM nurse_patients AS np
		JOIN patients as p ON np.patient_id = p.id
		JOIN employees as e ON np.employee_id = e.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting patient list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id               sql.NullString
			full_name        sql.NullString
			nurse            sql.NullString
			birthday         sql.NullString
			gender           sql.NullString
			age              sql.NullInt64
			height           sql.NullString
			weight           sql.NullString
			phone            sql.NullString
			home_phone       sql.NullString
			address          sql.NullString
			apartment_number sql.NullString
			latitude         sql.NullString
			longitude        sql.NullString
			enterance        sql.NullString
			floor            sql.NullString
			home_number      sql.NullString
			created_at       sql.NullString
			updated_at       sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
			&nurse,
			&birthday,
			&gender,
			&age,
			&height,
			&weight,
			&phone,
			&home_phone,
			&address,
			&apartment_number,
			&latitude,
			&longitude,
			&enterance,
			&floor,
			&home_number,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Patient = append(resp.Patient, &models.Patient{
			Id:               id.String,
			Full_name:        full_name.String,
			Nurse:            nurse.String,
			Latitude:         latitude.String,
			Longitude:        longitude.String,
			Age:              int(age.Int64),
			Birthday:         birthday.String,
			Gender:           gender.String,
			Height:           height.String,
			Weight:           weight.String,
			Phone:            phone.String,
			HomePhone:        home_phone.String,
			Address:          address.String,
			Apartment_number: apartment_number.String,
			Enterance:        enterance.String,
			Floor:            floor.String,
			Home_number:      home_number.String,
			Created_at:       created_at.String,
			Updated_at:       updated_at.String,
		})
	}

	return resp, nil
}
