package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type patientDiagnosisRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewPatientDiagnosisRepo(db *pgxpool.Pool, log logger.LoggerI) *patientDiagnosisRepo {
	return &patientDiagnosisRepo{
		db:  db,
		log: log,
	}
}

func (u *patientDiagnosisRepo) Create(ctx context.Context, req *models.PatientDiagnosisCreate) (*models.PatientDiagnosis, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO patient_procedures (
			id,
			patient_id,
			diagnosis_id,
			procedure_id,
			time,
			comment,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)
	_, err = u.db.Exec(ctx, query, id, req.PatientID, req.DiagnosisID, req.ProcedureID, req.Time, req.Comment, currentTime)
	if err != nil {
		u.log.Error("error is while creating patientDiagnosis data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.PatientDiagnosisPrimaryKey{ID: id})
	if err != nil {
		u.log.Error("error get by id patientDiagnosis" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *patientDiagnosisRepo) GetByID(ctx context.Context, req *models.PatientDiagnosisPrimaryKey) (*models.PatientDiagnosis, error) {
	var (
		query        string
		id           sql.NullString
		patient_id   sql.NullString
		diagnosis_id sql.NullString
		procedure_id sql.NullString
		created_at   sql.NullString
	)

	query = `
		SELECT 
			id,
			patient_id,
			diagnosis_id,
			procedure_id,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "patient_procedures" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.ID).Scan(
		&id,
		&patient_id,
		&diagnosis_id,
		&procedure_id,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.PatientDiagnosis{
		ID:          id.String,
		PatientID:   patient_id.String,
		DiagnosisID: diagnosis_id.String,
		ProcedureID: procedure_id.String,
		CreatedAt:   created_at.String,
	}, nil
}

func (u *patientDiagnosisRepo) GetList(ctx context.Context, req *models.PatientDiagnosisGetListRequest) (*models.PatientDiagnosisGetListResponse, error) {
	var (
		resp   = &models.PatientDiagnosisGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			patient_id,
			diagnosis_id,
			procedure_id,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "patient_procedures"
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting patientDiagnosis list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			patient_id   sql.NullString
			diagnosis_id sql.NullString
			procedure_id sql.NullString
			created_at   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&patient_id,
			&diagnosis_id,
			&procedure_id,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.PatientDiagnosis = append(resp.PatientDiagnosis, &models.PatientDiagnosis{
			ID:          id.String,
			PatientID:   patient_id.String,
			DiagnosisID: diagnosis_id.String,
			ProcedureID: procedure_id.String,
			CreatedAt:   created_at.String,
		})
	}
	return resp, nil
}

// func (u *patientDiagnosisRepo) Update(ctx context.Context, req *models.PatientDiagnosisUpdate) (int64, error) {
// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			"patientDiagnosis"
// 		SET
// 			employee_id = :employee_id,
// 			patient_id = :patient_id,
// 			comment = :comment,
// 			count = :count,
// 			update = :updated_at
// 		WHERE id = :id
// 	`

// 	timeZone, err := time.LoadLocation("Asia/Tashkent")
// 	if err != nil {
// 		u.log.Error("error is while loading tashkent timezone" + err.Error())
// 		return 0, err
// 	}

// 	currentTime := time.Now().In(timeZone)

// 	params = map[string]interface{}{
// 		"id":          req.Id,
// 		"employee_id": req.PatientDiagnosisId,
// 		"patient_id":  req.PatientId,
// 		"comment":     req.Comment,
// 		"count":       req.Count,
// 		"updated_at":  currentTime,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	result, err := u.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		u.log.Error("error is while updating patientDiagnosis data", logger.Error(err))
// 		return 0, err
// 	}

// 	return result.RowsAffected(), nil
// }

func (u *patientDiagnosisRepo) Delete(ctx context.Context, req *models.PatientDiagnosisPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM patient_procedures   WHERE id = $1`, req.ID)
	if err != nil {
		u.log.Error("error is while deleting patientDiagnosis", logger.Error(err))
		return err
	}

	return nil
}

func (u *patientDiagnosisRepo) GetDiagnosisProceduresByDiagnosisID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.PatientDiagnosisGetListResponse, error) {
	var (
		resp  = &models.PatientDiagnosisGetListResponse{}
		query string
	)

	query = `
		SELECT 
			p.name,
			pp.time,
			pp.comment
		FROM "patient_procedures" AS pp
		JOIN "procedures" AS p ON pp.procedure_id = p.id
		WHERE pp.diagnosis_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting patientDiagnosis list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			name    sql.NullString
			time    sql.NullString
			comment sql.NullString
		)
		err = rows.Scan(
			&name,
			&time,
			&comment,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.PatientDiagnosis = append(resp.PatientDiagnosis, &models.PatientDiagnosis{
			ProcedureName: name.String,
			Time:          time.String,
			Comment:       comment.String,
		})
	}

	return resp, nil
}
