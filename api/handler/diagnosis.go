package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Diagnosiss godoc
// @ID create_diagnosiss
// @Router /clinic/api/v1/diagnosis [POST]
// @Summary Create Diagnosiss
// @Description Create Diagnosiss
// @Tags Diagnosis
// @Accept json
// @Diagnosis json
// @Param Diagnosis body models.DiagnosisCreate true "CreateDiagnosisRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateDiagnosis(c *gin.Context) {
	var (
		diagnosisCreate models.DiagnosisCreate
	)

	err := c.ShouldBindJSON(&diagnosisCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Diagnosis Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Diagnosis().Create(c.Request.Context(), &diagnosisCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Diagnosis.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	fmt.Println(diagnosisCreate.Procedure)

	for _, procedure := range diagnosisCreate.Procedure {
		_, err := h.storage.PatientDiagnosis().Create(c.Request.Context(), &models.PatientDiagnosisCreate{
			PatientID:   diagnosisCreate.PatientId,
			DiagnosisID: resp.Id,
			ProcedureID: procedure.ProcedureId,
			Time:        procedure.Time,
			Comment:     procedure.Comment,
		})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error PatientDiagnosis.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	_,err = h.storage.Treatment().Create(c.Request.Context(), &models.TreatmentCreate{
		DiagnosisId: resp.Id,
		EmployeeId: diagnosisCreate.EmployeeID,	
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Treatment.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Diagnosis Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Diagnosiss godoc
// @ID get_by_id_diagnosiss
// @Router /clinic/api/v1/diagnosis/{id} [GET]
// @Summary Get By ID Diagnosiss
// @Description Get By ID Diagnosiss
// @Tags Diagnosis
// @Accept json
// @Diagnosis json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdDiagnosis(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Diagnosis().GetByID(c.Request.Context(), &models.DiagnosisPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Diagnosis.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	procedures,err := h.storage.PatientDiagnosis().GetDiagnosisProceduresByDiagnosisID(c.Request.Context(),&models.DiagnosisPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.GetDiagnosisProceduresByDiagnosisID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	request.Procedure = procedures.PatientDiagnosis

	h.logger.Info("GetByID Diagnosis Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Diagnosiss godoc
// @ID get_list_diagnosiss
// @Router /clinic/api/v1/diagnosiss [GET]
// @Summary Get List Diagnosiss
// @Description Get List Diagnosiss
// @Tags Diagnosis
// @Accept json
// @Diagnosis json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Param patient_id query string false "patient_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListDiagnosis(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListDiagnosis INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListDiagnosis INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Diagnosis().GetList(c.Request.Context(), &models.DiagnosisGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Diagnosis.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListDiagnosis Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Diagnosiss godoc
// @ID update_diagnosiss
// @Router /clinic/api/v1/diagnosis/{id} [PUT]
// @Summary Update Diagnosiss
// @Description Update Diagnosiss
// @Tags Diagnosis
// @Accept json
// @Diagnosis json
// @Param id path string true "id"
// @Param Diagnosis body models.DiagnosisUpdate true "UpdateDiagnosisRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateDiagnosis(c *gin.Context) {
	var (
		id              = c.Param("id")
		diagnosisUpdate models.DiagnosisUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&diagnosisUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Diagnosis Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	diagnosisUpdate.Id = id
	rowsAffected, err := h.storage.Diagnosis().Update(c.Request.Context(), &diagnosisUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Diagnosis.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Diagnosis.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Diagnosis().GetByID(c.Request.Context(), &models.DiagnosisPrimaryKey{Id: diagnosisUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Diagnosis.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Diagnosis Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Diagnosiss godoc
// @ID delete_diagnosiss
// @Router /clinic/api/v1/diagnosis/{id} [DELETE]
// @Summary Delete Diagnosiss
// @Description Delete Diagnosiss
// @Tags Diagnosis
// @Accept json
// @Diagnosis json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteDiagnosis(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Diagnosis().Delete(c.Request.Context(), &models.DiagnosisPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Diagnosis.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Diagnosis Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
