package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type salaryRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewSalaryRepo(db *pgxpool.Pool, log logger.LoggerI) *salaryRepo {
	return &salaryRepo{
		db:  db,
		log: log,
	}
}

func (u *salaryRepo) Create(ctx context.Context, req *models.SalaryCreate) (*models.Salary, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO salary (
			id,
			employee_id,
			cash,
			card,
			comment,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Employee_id, req.Cash, req.Card, req.Comment, currentTime)
	if err != nil {
		u.log.Error("error is while creating salary data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.SalaryPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id salary" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *salaryRepo) GetByID(ctx context.Context, req *models.SalaryPrimaryKey) (*models.Salary, error) {
	var (
		query       string
		id          sql.NullString
		employee_id sql.NullString
		cash        sql.NullFloat64
		card        sql.NullFloat64
		comment     sql.NullString
		created_at  sql.NullString
	)

	query = `
		SELECT 
			id,
			employee_id,
			cash,
			card,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy') 
		FROM "salary" 
		WHERE deleted_at is null AND id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&employee_id,
		&cash,
		&card,
		&comment,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Salary{
		Id:          id.String,
		Employee_id: employee_id.String,
		Cash:        cash.Float64,
		Card:        card.Float64,
		Comment:     comment.String,
		Created_at:  created_at.String,
	}, nil
}

func (u *salaryRepo) GetList(ctx context.Context, req *models.SalaryGetListRequest) (*models.SalaryGetListResponse, error) {
	var (
		resp   = &models.SalaryGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			employee_id,
			cash,
			card,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "salary" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting salary list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			employee_id sql.NullString
			cash        sql.NullFloat64
			card        sql.NullFloat64
			comment     sql.NullString
			created_at  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&employee_id,
			&cash,
			&card,
			&comment,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Salary = append(resp.Salary, &models.Salary{
			Id:          id.String,
			Employee_id: employee_id.String,
			Cash:        cash.Float64,
			Card:        card.Float64,
			Comment:     comment.String,
			Created_at:  created_at.String,
		})
	}
	return resp, nil
}

func (u *salaryRepo) Delete(ctx context.Context, req *models.SalaryPrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE salarys SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting salarys", logger.Error(err))
		return err
	}

	return nil
}
