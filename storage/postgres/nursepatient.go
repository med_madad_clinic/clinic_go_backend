package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type nursePatientRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewNursePatientRepo(db *pgxpool.Pool, log logger.LoggerI) *nursePatientRepo {
	return &nursePatientRepo{
		db:  db,
		log: log,
	}
}

func (u *nursePatientRepo) Create(ctx context.Context, req *models.NursePatientCreate) error {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO nurse_patients(
			id,
			employee_id,
			patient_id,
			created_at
		)
		VALUES($1, $2, $3, $4)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.EmployeeID, req.PatientId, currentTime)
	if err != nil {
		u.log.Error("error is while creating nursePatient data" + err.Error())
		return err
	}

	return nil
}

func (u *nursePatientRepo) Delete(ctx context.Context, req *models.DeleteNursePatient) error {

	_, err := u.db.Exec(ctx, `DELETE from nurse_patients WHERE employee_id = $1 AND patient_id = $2`, req.EmployeeID, req.PatientId)
	if err != nil {
		u.log.Error("error is while deleting nursePatients", logger.Error(err))
		return err
	}

	return nil
}

func (u *nursePatientRepo) GetNursePatients(ctx context.Context, req *models.EmployeePrimaryKey) (*models.PatientGetListResponse, error) {

	var resp = &models.PatientGetListResponse{}
	query := `
		SELECT 
			COUNT(*) OVER(),
			p.id,
			p.full_name,
			p.age,
			p.address,
			p.phone
		FROM patients AS p
		JOIN nurse_patients AS np ON p.id = np.patient_id
		WHERE np.employee_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting patients list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			full_name sql.NullString
			age       sql.NullInt32
			address   sql.NullString
			phone     sql.NullString
		)
		if err := rows.Scan(&resp.Count,&id, &full_name, &age, &address, &phone); err != nil {
			u.log.Error("error is while scanning patients list", logger.Error(err))
			return nil, err
		}

		resp.Patient = append(resp.Patient, &models.Patient{
			Id: id.String,
			Full_name: full_name.String,
			Age:       int(age.Int32),
			Address:   address.String,
			Phone:     phone.String,
		})
	}

	return resp, nil
}
