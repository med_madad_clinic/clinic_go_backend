package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type procedureRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewProcedureRepo(db *pgxpool.Pool, log logger.LoggerI) *procedureRepo {
	return &procedureRepo{
		db:  db,
		log: log,
	}
}

func (u *procedureRepo) Create(ctx context.Context, req *models.ProcedureCreate) (*models.Procedure, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO procedures (
			id,
			category_id,
			name,
			description,
			nurse_share,
			created_at
		)
		VALUES($1, $2, $3, $4,$5,$6)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.CategoryId, req.Name, req.Description, req.NurseShare, currentTime)
	if err != nil {
		u.log.Error("error is while creating procedure data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ProcedurePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id procedure" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *procedureRepo) GetByID(ctx context.Context, req *models.ProcedurePrimaryKey) (*models.Procedure, error) {
	var (
		query       string
		id          sql.NullString
		category_id sql.NullString
		name        sql.NullString
		description sql.NullString
		nurse_share sql.NullFloat64
		created_at  sql.NullString
	)

	query = `
		SELECT 
			id,
			category_id,
			name,
			description,
			nurse_share,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "procedures" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&name,
		&description,
		&nurse_share,
		&created_at,
	)
	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Procedure{
		Id:          id.String,
		CategoryId:  category_id.String,
		Name:        name.String,
		Description: description.String,
		NurseShare:  nurse_share.Float64,
		Created_at:  created_at.String,
	}, nil
}

func (u *procedureRepo) GetList(ctx context.Context, req *models.ProcedureGetListRequest) (*models.ProcedureGetListResponse, error) {
	var (
		resp   = &models.ProcedureGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY p.created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			c.name,
			p.name,
			p.description,
			p.nurse_share,
			TO_CHAR(p.created_at,'dd/mm/yyyy')
		FROM "procedures" AS p
		JOIN "categories" AS c ON c.id = p.category_id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting procedure list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id            sql.NullString
			category_name sql.NullString
			name          sql.NullString
			description   sql.NullString
			nurse_share   sql.NullFloat64
			created_at    sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&category_name,
			&name,
			&description,
			&nurse_share,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Procedure = append(resp.Procedure, &models.Procedure{
			Id:           id.String,
			CategoryName: category_name.String,
			Name:         name.String,
			Description:  description.String,
			NurseShare:   nurse_share.Float64,
			Created_at:   created_at.String,
			Order:        order,
		})
		order++
	}
	return resp, nil
}

func (u *procedureRepo) Update(ctx context.Context, req *models.ProcedureUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"procedures"
		SET
			name = :name,
			description = :description,
			nurse_share = :nurse_share,
			update = :updated_at
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":          req.Id,
		"name":        req.Name,
		"description": req.Description,
		"nurse_share": req.NurseShare,
		"updated_at":  currentTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating procedure data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *procedureRepo) Delete(ctx context.Context, req *models.ProcedurePrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE from treatment_procedures WHERE procedure_id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting treatment_procedures", logger.Error(err))
		return err
	}

	_, err = u.db.Exec(ctx, `DELETE from procedures WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting procedures", logger.Error(err))
		return err
	}

	return nil
}

func (u *procedureRepo) GetProcdureByCategory(ctx context.Context) (*models.GetProcdureByCategory, error) {
	var (
		query string
		resp  = &models.GetProcdureByCategory{}
	)

	query = `
		SELECT
			distinct(c.id),
			c.name
		FROM "categories" AS c
		JOIN "procedures" AS p ON p.category_id = c.id
	`

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting procedure list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			procedure = &models.ProceduresByCategory{}
		)
		err = rows.Scan(
			&id,
			&name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		query1 := `
				SELECT 
					id,
					name
				FROM "procedures"
				WHERE category_id = $1
			`
		rows1, err := u.db.Query(ctx, query1, id.String)
		if err != nil {
			u.log.Error("error is while getting procedure list" + err.Error())
			return nil, err
		}

		for rows1.Next() {
			var (
				procedure_id   sql.NullString
				procedure_name sql.NullString
			)
			err = rows1.Scan(
				&procedure_id,
				&procedure_name,
			)
			if err != nil {
				u.log.Error("error is while getting user list (scanning data)" + err.Error())
				return nil, err
			}

			procedure.Procedures = append(procedure.Procedures, &models.Procedure{
				Id:   procedure_id.String,
				Name: procedure_name.String,
			})

			procedure.CategoryName = name.String

		}

		resp.Data = append(resp.Data, procedure)

		name.String = ""

	}

	return resp, nil
}
