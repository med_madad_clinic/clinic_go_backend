package models

type NursePatient struct {
	Id         string  `json:"id,omitempty"`
	EmployeeID string  `json:"employee_id,omitempty"`
	PatientId  string  `json:"patient,omitempty"`
	StartDate  string  `json:"date"`
	Days       []*Days `json:"days"`
	StartTime  string  `json:"start_time,omitempty"`
	Created_at string  `json:"created_at,omitempty"`
}

type Days struct {
	Day  string `json:"day,omitempty"`
}



type NursePatientCreate struct {
	EmployeeID string  `json:"employee_id,omitempty"`
	PatientId  string  `json:"patient_id,omitempty"`
	StartDate  string  `json:"start_date"`
	Days       []*Days `json:"days"`
	StartTime  string  `json:"start_time,omitempty"`
	EndTime    string  `json:"end_time,omitempty"`
}

type DeleteNursePatient struct {
	EmployeeID string `json:"employee_id"`
	PatientId  string `json:"patient_id"`
}

