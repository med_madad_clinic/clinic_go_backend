package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

type dashboardRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewDashboardRepo(db *pgxpool.Pool, log logger.LoggerI) *dashboardRepo {
	return &dashboardRepo{
		db:  db,
		log: log,
	}
}

func (u *dashboardRepo) GetListConsultations(ctx context.Context) (*models.ConsultationGetListResponse, error) {
	var (
		resp  = &models.ConsultationGetListResponse{}
		query string
		order = 1
	)

	query = `
	SELECT
		COUNT(*) OVER(),
		c.id,
		p.full_name,
		p.id,
		TO_CHAR(c.date, 'YYYY-MM-DD'),
		p.gender,
		c.type,
		p.age
	FROM "consultations" AS c
	JOIN "patients" AS p ON c.patient_id = p.id
	ORDER BY c.created_at ASC
	`
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting consultation list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id                sql.NullString
			patient_id        sql.NullString
			date              sql.NullString
			full_name         sql.NullString
			gender            sql.NullString
			consultation_type sql.NullString
			age               sql.NullInt64
		)
		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
			&patient_id,
			&date,
			&gender,
			&consultation_type,
			&age,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.Consultation = append(resp.Consultation, &models.Consultation{
			Id:          id.String,
			PatientName: full_name.String,
			PatientId:   patient_id.String,
			Gender:      gender.String,
			Type:        consultation_type.String,
			Age:         age.Int64,
			Date:        date.String,
			Order:       order,
		})
		order++
	}
	return resp, nil
}

func (u *dashboardRepo) ScheduleToday(ctx context.Context, date string) (*models.GetListTodayScheduleResponse, error) {
	var (
		query string
	)

	currentDate, err := time.Parse("2006-01-02", date)
	if err != nil {
		u.log.Error("error is while parsing date" + err.Error())
		return nil, err
	}

	day := currentDate.Weekday()

	query = `
		SELECT 
			s.employee_id,
			p.id,		
			e.full_name,
			p.full_name,
			s.start_time,
			s.end_time
		FROM "schedule" AS s
		JOIN "employees" AS e ON s.employee_id = e.id
		JOIN "patients" AS p ON s.patient_id = p.id
		WHERE s.day =$1 AND s.start_date <= $2
		ORDER BY s.start_time ASC 
	`
	rows, err := u.db.Query(ctx, query, day, currentDate)
	if err != nil {
		u.log.Error("error is while getting user list", logger.Error(err))
		return nil, err
	}

	resp := &models.GetListTodayScheduleResponse{}
	for rows.Next() {
		var (
			employee_id   sql.NullString
			patient_id    sql.NullString
			employee_name sql.NullString
			patient_name  sql.NullString
			start_time    sql.NullString
			end_time      sql.NullString
		)
		err = rows.Scan(
			&employee_id,
			&patient_id,
			&employee_name,
			&patient_name,
			&start_time,
			&end_time,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.Schedule = append(resp.Schedule, &models.TodaySchedule{
			EmployeeId:   employee_id.String,
			PatientId:    patient_id.String,
			EmployeeName: employee_name.String,
			PatientName:  patient_name.String,
			StartTime:    start_time.String,
			EndTime:      end_time.String,
		})
	}

	return resp, nil

}

func (u *dashboardRepo) PatientStatistics(ctx context.Context) (*models.PatientDashoard, error) {

	var (
		query string
		resp  = &models.PatientDashoard{}
	)

	weekStart, weekEnd := getWeekStartAndEnd()
	query = `
		SELECT 
    		(SELECT COUNT(*) FROM patients),
    		(SELECT COUNT(*) FROM patients WHERE TO_CHAR(created_at,'yyyy-mm-dd') BETWEEN $1 AND $2),
    		(SELECT COUNT(*) FROM patients WHERE gender = 'man'),
			(SELECT COUNT(*) FROM patients WHERE gender = 'woman')
	`

	rows, err := u.db.Query(ctx, query, weekStart, weekEnd)
	if err != nil {
		u.log.Error("error is while getting user list", logger.Error(err))
		return nil, err
	}
	for rows.Next() {
		var (
			total       sql.NullInt64
			man         sql.NullInt64
			woman       sql.NullInt64
			new_patient sql.NullInt64
		)
		err = rows.Scan(
			&total,
			&new_patient,
			&man,
			&woman,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}
		resp.Man = int(man.Int64)
		resp.Woman = int(woman.Int64)
		resp.TotalCount = int(total.Int64)
		resp.NewPatient = int(new_patient.Int64)

	}

	return resp, nil
}

func (u *dashboardRepo) TotalCount(ctx context.Context) (*models.TotalCount, error) {
	var (
		query     string
		treatment sql.NullInt16
		patient   sql.NullInt16
		doctor    sql.NullInt16
		employee  sql.NullInt16
		weekResp  = &models.Week{}
		monthResp = &models.Month{}
		yearResp  = &models.Year{}
	)

	query = `
	SELECT 
    (SELECT COUNT(*) FROM patients),
    (SELECT COUNT(*) FROM treatment),
    (SELECT COUNT(*) FROM employees),
	(SELECT COUNT(*) FROM employees WHERE role_id = (SELECT id FROM roles WHERE role_name = 'Shifokor'))	
	`

	weekQuery := `
			WITH date_range AS (
				SELECT generate_series(
					$1::date, 
					$2::date, 
					'1 day'::interval
				)::date AS day
			)
			SELECT 
				COALESCE(COUNT(p.id), 0) AS count,
				TO_CHAR(dr.day, 'day') AS date
			FROM date_range dr
			LEFT JOIN patients p ON p.created_at::date = dr.day
			GROUP BY dr.day
			ORDER BY dr.day;
	`

	monthQuery := `
				WITH date_range AS (
					SELECT generate_series(
						$1::date,
						$2::date,
						'1 day'::interval
					)::date AS day
				)
				SELECT
					COALESCE(COUNT(p.id), 0) AS count,
					TO_CHAR(dr.day, 'dd') AS date
				FROM date_range dr
				LEFT JOIN patients p ON p.created_at::date = dr.day
				GROUP BY dr.day
				ORDER BY dr.day;
		`

	yearQuery := `
			WITH month_range AS (
				SELECT generate_series(
					$1::date,
					$2::date,
					'1 month'::interval
				) AS month_start
			)
			SELECT
				COALESCE(COUNT(p.id), 0) AS count,
				TO_CHAR(mr.month_start, 'Month') AS month
			FROM month_range mr
			LEFT JOIN patients p ON p.created_at::date >= mr.month_start AND p.created_at::date < mr.month_start + INTERVAL '1 month'
			GROUP BY mr.month_start
			ORDER BY mr.month_start;
	`

	startweek, endweek := getWeekStartAndEnd()

	startdate, enddate := getMonthStartAndEnd()

	startyear, endyear := getYearStartAndEnd()

	err := u.db.QueryRow(ctx, query).Scan(
		&treatment,
		&patient,
		&employee,
		&doctor,
	)
	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	row, err := u.db.Query(ctx, weekQuery, startweek, endweek)
	if err != nil {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}
	for row.Next() {
		var (
			count int
			day   sql.NullString
		)
		err = row.Scan(
			&count,
			&day,
		)
		if err != nil {
			u.log.Error("error while scanning data" + err.Error())
			return nil, err
		}

		weekResp.Data = append(weekResp.Data, &models.Data{
			Label:            day.String,
			NumberOfPatients: int64(count),
		})

	}

	row, err = u.db.Query(ctx, monthQuery, startdate, enddate)
	if err != nil {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}
	for row.Next() {
		var (
			count int
			day   sql.NullString
		)
		err = row.Scan(
			&count,
			&day,
		)
		if err != nil {
			u.log.Error("error while scanning data" + err.Error())
			return nil, err
		}
		monthResp.Data = append(monthResp.Data, &models.Data{
			Label:            day.String,
			NumberOfPatients: int64(count),
		})
	}

	year, err := u.db.Query(ctx, yearQuery, startyear, endyear)
	if err != nil {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}
	for year.Next() {
		var (
			count int
			day   sql.NullString
		)
		err = year.Scan(
			&count,
			&day,
		)
		if err != nil {
			u.log.Error("error while scanning data" + err.Error())
			return nil, err
		}
		yearResp.Data = append(yearResp.Data, &models.Data{
			Label:            day.String,
			NumberOfPatients: int64(count),
		})
	}
	return &models.TotalCount{
		Treatment: int(treatment.Int16),
		Patient:   int(patient.Int16),
		Doctor:    int(doctor.Int16),
		Employee:  int(employee.Int16),
		Chart: &models.Chart{
			Week:  weekResp,
			Month: monthResp,
			Year:  yearResp,
		},
	}, nil
}

func getWeekStartAndEnd() (string, string) {
	// Get the current time
	now := time.Now()

	// Find the start of the week (Monday)
	weekday := int(now.Weekday())
	if weekday == 0 {
		weekday = 7 // Sunday is the last day of the week
	}
	startOfWeek := now.AddDate(0, 0, -weekday+1)

	// Find the end of the week (Sunday)
	endOfWeek := startOfWeek.AddDate(0, 0, 6)

	// Format the dates
	dateFormat := "2006-01-02"
	startOfWeekStr := startOfWeek.Format(dateFormat)

	endOfWeekStr := endOfWeek.Format(dateFormat)

	return startOfWeekStr, endOfWeekStr
}

func getMonthStartAndEnd() (string, string) {
	day := time.Now().Day()
	now := time.Now()

	startOfMonth := now.AddDate(0, 0, -day+1)

	startOfMonthStr := startOfMonth.Format("2006-01-02")

	endOfMonth := startOfMonth.AddDate(0, 1, -1)
	endOfMonthStr := endOfMonth.Format("2006-01-02")

	return startOfMonthStr, endOfMonthStr

}

func getYearStartAndEnd() (string, string) {
	_, month, day := time.Now().Date()

	now := time.Now()

	startOfYear := now.AddDate(0, int(-month)+1, -day+1)

	startOfYearStr := startOfYear.Format("2006-01-02")

	endOfYear := startOfYear.AddDate(1, 0, -1)
	endOfYearStr := endOfYear.Format("2006-01-02")

	return startOfYearStr, endOfYearStr

}
