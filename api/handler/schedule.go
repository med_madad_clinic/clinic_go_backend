package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Schedules godoc
// @ID create_schedule
// @Router /clinic/api/v1/dchedule [POST]
// @Summary Create Schedules
// @Description Create Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param Schedule body models.ScheduleCreate true "CreateScheduleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateSchedule(c *gin.Context) {
	var (
		dcheduleCreate models.ScheduleCreate
	)

	err := c.ShouldBindJSON(&dcheduleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Schedule Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Schedule().Create(c.Request.Context(), &dcheduleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Schedule.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Schedule Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Schedules godoc
// @ID get_by_id_schedule
// @Router /clinic/api/v1/dchedule/{id} [GET]
// @Summary Get By ID Schedules
// @Description Get By ID Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdSchedule(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Schedule().GetByID(c.Request.Context(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Schedule Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Schedules godoc
// @ID get_list_schedule
// @Router /clinic/api/v1/schedule [GET]
// @Summary Get List Schedules
// @Description Get List Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListSchedule(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSchedule INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSchedule INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Schedule().GetList(c.Request.Context(), &models.ScheduleGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListSchedule Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Schedules godoc
// @ID update_schedule
// @Router /clinic/api/v1/dchedule/{id} [PUT]
// @Summary Update Schedules
// @Description Update Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param id path string true "id"
// @Param Schedule body models.ScheduleUpdate true "UpdateScheduleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
// func (h *handler) UpdateSchedule(c *gin.Context) {
// 	var (
// 		id             = c.Param("id")
// 		dcheduleUpdate models.ScheduleUpdate
// 	)

// 	if !helper.IsValidUUID(id) {
// 		h.logger.Error("is invalid uuid!")
// 		c.JSON(http.StatusBadRequest, "invalid id")
// 		return
// 	}

// 	err := c.ShouldBindJSON(&dcheduleUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "error Schedule Should Bind Json!")
// 		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
// 		return
// 	}

// 	dcheduleUpdate.Id = id
// 	rowsAffected, err := h.storage.Schedule().Update(c.Request.Context(), &dcheduleUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.Update!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	if rowsAffected <= 0 {
// 		h.logger.Error("storage.Schedule.Update!")
// 		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
// 		return
// 	}

// 	resp, err := h.storage.Schedule().GetByID(c.Request.Context(), &models.SchedulePrimaryKey{Id: dcheduleUpdate.Id})
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetByID!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	h.logger.Info("Update Schedule Successfully!")
// 	c.JSON(http.StatusAccepted, resp)
// }

// Delete Schedules godoc
// @ID delete_schedule
// @Router /clinic/api/v1/dchedule/{id} [DELETE]
// @Summary Delete Schedules
// @Description Delete Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteSchedule(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Schedule().Delete(c.Request.Context(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Schedule Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}




// GetList Employee Schedules godoc
// @ID get_list_employee_schedule
// @Router /clinic/api/v1/employee-schedule [GET]
// @Summary Get List Employee Schedules
// @Description Get List Employee Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListEmlpoyeeSchedule(c *gin.Context) {

	date := c.Query("date")


	resp, err := h.storage.Schedule().GetEmployeesSchedule(c.Request.Context(), &models.GetEmployeesScheduleRequest{
		Date: date,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.GetListEmlpoyeeSchedule!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListSchedule Response!")
	c.JSON(http.StatusOK, resp)
}



// GetList nurse Schedules godoc
// @ID get_list_nurse_schedule
// @Router /clinic/api/v1/nurse-schedule [GET]
// @Summary Get List nurse Schedules
// @Description Get List nurse Schedules
// @Tags Schedule
// @Accept json
// @Schedule json
// @Param date query string false "date"
// @Param employee_id query string false "emloyee_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListNurseSchedule(c *gin.Context) {

	date := c.Query("date")
	employee_id := c.Query("employee_id")

	resp, err := h.storage.Schedule().GetNurseSchedule(c.Request.Context(), &models.GetEmployeesScheduleRequest{
		Date: date,
		EmployeeId: employee_id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.GetListEmlpoyeeSchedule!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListSchedule Response!")
	c.JSON(http.StatusOK, resp)
}

