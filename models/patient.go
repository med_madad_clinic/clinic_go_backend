package models

type Patient struct {
	Id                string           `json:"id,omitempty"`
	EmployeeID        string           `json:"employee_id,omitempty"`
	ArrivalTime       string           `json:"arrival_time,omitempty"`
	LeavingTime       string           `json:"leaving_time,omitempty"`
	CounsultationDate string           `json:"counsultation_date,omitempty"`
	Full_name         string           `json:"full_name,omitempty"`
	Latitude          string           `json:"latitude,omitempty"`
	Longitude         string           `json:"longitude,omitempty"`
	Age               int              `json:"age,omitempty"`
	Birthday          string           `json:"birthday,omitempty"`
	Gender            string           `json:"gender,omitempty"`
	Height            string           `json:"height,omitempty"`
	Weight            string           `json:"weight,omitempty"`
	Phone             string           `json:"phone,omitempty"`
	HomePhone         string           `json:"home_phone,omitempty"`
	Address           string           `json:"address,omitempty"`
	Apartment_number  string           `json:"apartment_number,omitempty"`
	Enterance         string           `json:"enterance,omitempty"`
	Floor             string           `json:"floor,omitempty"`
	Home_number       string           `json:"home_number,omitempty"`
	Consultation_type string           `json:"consultation_type,omitempty"`
	Created_at        string           `json:"created_at,omitempty"`
	Updated_at        string           `json:"updated_at,omitempty"`
	Deleted_at        string           `json:"deleted_at,omitempty"`
	Questionnaire     []*Questionnaire `json:"questionnaire,omitempty"`
	Url               []*Url           `json:"url,omitempty"`
	Start_time        string           `json:"start_time,omitempty"`
	End_time          string           `json:"end_time,omitempty"`
	Doctor            string           `json:"doctor,omitempty"`
	Nurse             string           `json:"nurse,omitempty"`
	Order             int64            `json:"order,omitempty"`
}

type PatientCreate struct {
	EmployeeID        string `json:"employee_id,omitempty"`
	ArrivalTime       string `json:"arrival_time,omitempty"`
	LeavingTime       string `json:"leaving_time,omitempty"`
	ConsultationDate  string `json:"consultation_date,omitempty"`
	Full_name         string `json:"full_name,omitempty"`
	Age               int    `json:"-"`
	Latitude          string `json:"latitude,omitempty"`
	Longitude         string `json:"longitude,omitempty"`
	Birthday          string `json:"birthday,omitempty"`
	Gender            string `json:"gender,omitempty"`
	Height            string `json:"height,omitempty"`
	Weight            string `json:"weight,omitempty"`
	Phone             string `json:"phone,omitempty"`
	HomePhone         string `json:"home_phone,omitempty"`
	Address           string `json:"address,omitempty"`
	Apartment_number  string `json:"apartment_number,omitempty"`
	Enterance         string `json:"enterance,omitempty"`
	Floor             string `json:"floor,omitempty"`
	Home_number       string `json:"home_number,omitempty"`
	Consultation_type string `json:"consultation_type,omitempty"`
	Url               []*Url `json:"url,omitempty"`
}

type PatientUpdate struct {
	Id                string `json:"id"`
	EmployeeID        string `json:"employee_id,omitempty"`
	ArrivalTime       string `json:"arrival_time,omitempty"`
	LeavingTime       string `json:"leaving_time,omitempty"`
	ConsultationDate  string `json:"consultation_date,omitempty"`
	Full_name         string `json:"full_name,omitempty"`
	Latitude          string `json:"latitude,omitempty"`
	Longitude         string `json:"longitude,omitempty"`
	Birthday          string `json:"birthday,omitempty"`
	Gender            string `json:"gender,omitempty"`
	Height            string `json:"height,omitempty"`
	Weight            string `json:"weight,omitempty"`
	Phone             string `json:"phone,omitempty"`
	HomePhone         string `json:"home_phone,omitempty"`
	Address           string `json:"address,omitempty"`
	Apartment_number  string `json:"apartment_number,omitempty"`
	Enterance         string `json:"enterance,omitempty"`
	Floor             string `json:"floor,omitempty"`
	Home_number       string `json:"home_number,omitempty"`
	Consultation_type string `json:"consultation_type,omitempty"`
	Url               []*Url `json:"url,omitempty"`
	Age               int    `json:"-"`
}

type PatientPrimaryKey struct {
	Id string `json:"id"`
}

type PatientGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type PatientGetListResponse struct {
	Count   int        `json:"count"`
	Patient []*Patient `json:"patients"`
}
