package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create NursePatients godoc
// @ID create_nursePatientss
// @Router /clinic/api/v1/nursepatients [POST]
// @Summary Create NursePatients
// @Description Create NursePatients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param Patient body models.NursePatientCreate true "CreatePatientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateNursePatient(c *gin.Context) {
	var (
		nursePatientsCreate models.NursePatientCreate
	)

	err := c.ShouldBindJSON(&nursePatientsCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Patient Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	err = h.storage.NursePatient().Create(c.Request.Context(), &nursePatientsCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.NursePatient.Create!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, day := range nursePatientsCreate.Days {
		_, err := h.storage.Schedule().Create(c.Request.Context(), &models.ScheduleCreate{
			Employee_id: nursePatientsCreate.EmployeeID,
			Patient_id:  nursePatientsCreate.PatientId,
			Day:         day.Day,
			Date:        nursePatientsCreate.StartDate,
			Start_time:  nursePatientsCreate.StartTime,
			End_time:    nursePatientsCreate.EndTime,
		})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Schedule.Create!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	h.logger.Info("Create Patient Successfully!!")
	c.JSON(http.StatusCreated, "success")
}

// Delete NursePatients godoc
// @ID delete_nursePatientss
// @Router /clinic/api/v1/nursepatients/{id} [DELETE]
// @Summary Delete NursePatients
// @Description Delete NursePatients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param employee_id query string true "employee_id"
// @Param patient_id query string true "patient_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteNursePatient(c *gin.Context) {
	var employee_id = c.Query("employee_id")
	patient_id := c.Query("patient_id")

	if !helper.IsValidUUID(employee_id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	if !helper.IsValidUUID(patient_id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.NursePatient().Delete(c.Request.Context(), &models.DeleteNursePatient{
		EmployeeID: employee_id,
		PatientId:  patient_id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Patient Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// Get NursePatients godoc
// @ID Get_nursePatientss
// @Router /clinic/api/v1/getlistnursepatients [GET]
// @Summary Get NursePatients
// @Description Get NursePatients
// @Tags Patient
// @Accept json
// @Procedure json
// @Param employee_id query string true "employee_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListNursePatients(c *gin.Context) {
	employee_id := c.Query("employee_id")

	resp, err := h.storage.NursePatient().GetNursePatients(c.Request.Context(), &models.EmployeePrimaryKey{
		Id: employee_id,
	})

	if err != nil && err.Error() == "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Patient.Delete!")
		c.JSON(http.StatusInternalServerError, "server error")
		return
	}

	c.JSON(http.StatusAccepted, resp)

}
