package models

type FinishedProcedures struct {
	Id          string `json:"id"`
	PatientId   string `json:"patient_id"`
	EmployeeId  string `json:"employee_id"`
	DiagnosisId string `json:"diagnosis_id"`
	CreatedAt   string `json:"created_at"`
}

type FinishedProceduresCreate struct {
	PatientId   string `json:"patient_id"`
	EmployeeId  string `json:"employee_id"`
	DiagnosisId string `json:"diagnosis_id"`
}

type FinishedProceduresUpdate struct {
	Id          string `json:"id"`
	PatientId   string `json:"patient_id"`
	EmployeeId  string `json:"employee_id"`
	DiagnosisId string `json:"diagnosis_id"`
}

type FinishedProceduresPrimaryKey struct {
	ID string `json:"id"`
}

type FinishedProceduresGetListRequest struct {
	Limit  int    `json:"limit"`
	Offset int    `json:"offset"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type FinishedProceduresGetListResponse struct {
	FinishedProcedures []FinishedProcedures `json:"finished_procedures"`
	Count              int                  `json:"count"`
}
