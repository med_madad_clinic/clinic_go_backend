package handler

import (
	"clinic/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// Login godoc
// @ID /auth/login
// @Router /clinic/api/v1/auth/login [POST]
// @Summary Login
// @Description Login
// @Tags Auth
// @Accept json
// @Procedure json
// @Param login body models.LoginUser true "LoginRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) Login(c *gin.Context) {

	var (
		loginUser models.LoginUser
		// credentails map[string]interface{}
		// resp1       models.LoginResponse
		// resp        *models.User
		// accessToken string
	)

	err := c.ShouldBindJSON(&loginUser)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error LOGIN user should bind json")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}
	if len(loginUser.Password) < 8 {
		h.logger.Error("Password length should be greater than 8")
		c.JSON(http.StatusBadRequest, "Password length should be greater than 8")
		return
	}
	employee, err := h.storage.Employee().GetByLogin(c.Request.Context(), &models.EmployeePrimaryKey{Id: loginUser.Login})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "   Mentor Get By login Error")
		c.JSON(500, "Server error")
		return
	}
	role, err := h.storage.Role().GetByID(c.Request.Context(), &models.RolePrimaryKey{Id: employee.Role_id})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "   Mentor Get By login Error")
		c.JSON(500, "Server error")
		return
	}

	if employee != nil {
		err = bcrypt.CompareHashAndPassword([]byte(employee.Password), []byte(loginUser.Password))
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error CompareHashAndPassword")
			c.JSON(http.StatusBadRequest, "Invalid Password!")
			return
		}
		employee.Password = ""
		employee.Role = role.Role_name
		employee.Role_id = ""
		c.JSON(200, employee)
		return
	}

	// if academy != nil {
	// 	err = bcrypt.CompareHashAndPassword([]byte(academy.Password), []byte(loginUser.Password))
	// 	if err != nil {
	// 		h.logger.Error(err.Error() + "  :  " + "error CompareHashAndPassword")
	// 		c.JSON(http.StatusBadRequest, "Invalid Password!")
	// 		return
	// 	}
	// 	academy.Password = ""
	// 	academy.Type = "Admin"
	// 	c.JSON(200, academy)
	// 	return
	// 	// credentails = map[string]interface{}{
	// 	// 	"mentor_id": academy.Id,
	// 	// 	"role_id":   academy.RoleId,
	// 	// }

	// 	// accessToken, err = helper.GenerateJWT(credentails, time.Hour*360, h.cfg.SecretKey)
	// 	// if err != nil {
	// 	// 	h.logger.Error(err.Error() + "  :  " + "error in tokens GENERATEJWT.LOGIN")
	// 	// 	c.JSON(http.StatusInternalServerError, "Server Error!")
	// 	// 	return
	// 	// }

	// 	// resp1 = models.LoginResponse{
	// 	// 	AccessToken: accessToken,
	// 	// 	Mentor:      *mentorby,
	// 	// }
	// 	// goto STEP
	// }
	// c.JSON(400, "Wrong login or password")
	// return

}
