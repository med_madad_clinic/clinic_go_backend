package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create FinishedProcedures godoc
// @ID create_finishedProcedures
// @Router /clinic/api/v1/finishedProcedure [POST]
// @Summary Create FinishedProcedures
// @Description Create FinishedProcedures
// @Tags FinishedProcedure
// @Accept json
// @FinishedProcedure json
// @Param FinishedProcedure body models.FinishedProceduresCreate true "CreateFinishedProcedureRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateFinishedProcedure(c *gin.Context) {
	var (
		finishedProcedureCreate models.FinishedProceduresCreate
	)

	err := c.ShouldBindJSON(&finishedProcedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error FinishedProcedure Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.FinishedProcedure().Create(c.Request.Context(), &finishedProcedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error FinishedProcedure.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create FinishedProcedure Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID FinishedProcedures godoc
// @ID get_by_id_finishedProcedures
// @Router /clinic/api/v1/finishedProcedure/{id} [GET]
// @Summary Get By ID FinishedProcedures
// @Description Get By ID FinishedProcedures
// @Tags FinishedProcedure
// @Accept json
// @FinishedProcedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIDFinishedProcedure(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.FinishedProcedure().GetByID(c.Request.Context(), &models.FinishedProceduresPrimaryKey{ID: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.FinishedProcedure.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID FinishedProcedure Response!")
	c.JSON(http.StatusOK, request)
}

// GetList FinishedProcedures godoc
// @ID get_list_finishedProcedures
// @Router /clinic/api/v1/finishedProcedures [GET]
// @Summary Get List FinishedProcedures
// @Description Get List FinishedProcedures
// @Tags FinishedProcedure
// @Accept json
// @FinishedProcedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListFinishedProcedure(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListFinishedProcedure INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListFinishedProcedure INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.FinishedProcedure().GetList(c.Request.Context(), &models.FinishedProceduresGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.FinishedProcedure.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListFinishedProcedure Response!")
	c.JSON(http.StatusOK, resp)
}

// Update FinishedProcedures godoc
// @ID update_finishedProcedures
// @Router /clinic/api/v1/finishedProcedure/{id} [PUT]
// @Summary Update FinishedProcedures
// @Description Update FinishedProcedures
// @Tags FinishedProcedure
// @Accept json
// @FinishedProcedure json
// @Param id path string true "id"
// @Param FinishedProcedure body models.FinishedProceduresUpdate true "UpdateFinishedProcedureRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
// func (h *handler) UpdateFinishedProcedure(c *gin.Context) {
// 	var (
// 		id                      = c.Param("id")
// 		finishedProcedureUpdate models.FinishedProceduresUpdate
// 	)

// 	if !helper.IsValidUUID(id) {
// 		h.logger.Error("is invalid uuid!")
// 		c.JSON(http.StatusBadRequest, "invalid id")
// 		return
// 	}

// 	err := c.ShouldBindJSON(&finishedProcedureUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "error FinishedProcedure Should Bind Json!")
// 		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
// 		return
// 	}

// 	finishedProcedureUpdate.ID = id
// 	rowsAffected, err := h.storage.FinishedProcedure().Update(c.Request.Context(), &finishedProcedureUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.FinishedProcedure.Update!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	if rowsAffected <= 0 {
// 		h.logger.Error("storage.FinishedProcedure.Update!")
// 		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
// 		return
// 	}

// 	resp, err := h.storage.FinishedProcedure().GetByID(c.Request.Context(), &models.FinishedProceduresPrimaryKey{ID: finishedProcedureUpdate.ID})
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.FinishedProcedure.GetByID!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	h.logger.Info("Update FinishedProcedure Successfully!")
// 	c.JSON(http.StatusAccepted, resp)
// }

// Delete FinishedProcedures godoc
// @ID delete_finishedProcedures
// @Router /clinic/api/v1/finishedProcedure/{id} [DELETE]
// @Summary Delete FinishedProcedures
// @Description Delete FinishedProcedures
// @Tags FinishedProcedure
// @Accept json
// @FinishedProcedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteFinishedProcedure(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.FinishedProcedure().Delete(c.Request.Context(), &models.FinishedProceduresPrimaryKey{ID: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.FinishedProcedure.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("FinishedProcedure Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
