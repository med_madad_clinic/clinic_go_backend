package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.LoggerI) *categoryRepo {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (u *categoryRepo) Create(ctx context.Context, req *models.CategoryCreate) (*models.Category, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO categories (
			id,
			name,
			created_at
		)
		VALUES($1, $2, $3)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Name, currentTime)
	if err != nil {
		u.log.Error("error is while creating category data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.CategoryPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id category" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *categoryRepo) GetByID(ctx context.Context, req *models.CategoryPrimaryKey) (*models.Category, error) {
	var (
		query      string
		id         sql.NullString
		name       sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT 
			id,
			name,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "categories" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Category{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: created_at.String,
	}, nil
}

func (u *categoryRepo) GetList(ctx context.Context, req *models.CategoryGetListRequest) (*models.CategoryGetListResponse, error) {
	var (
		resp   = &models.CategoryGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "categories" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting category list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			name       sql.NullString
			created_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Category = append(resp.Category, &models.Category{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: created_at.String,
		})
	}
	return resp, nil
}

func (u *categoryRepo) Update(ctx context.Context, req *models.CategoryUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"categories"
		SET
			name = :name,
			update = :updated_at
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":   req.Id,
		"name": req.Name,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating category data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *categoryRepo) Delete(ctx context.Context, req *models.CategoryPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE from categories WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting categories", logger.Error(err))
		return err
	}

	return nil
}
