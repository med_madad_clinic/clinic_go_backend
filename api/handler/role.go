package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Roles godoc
// @ID create_roles
// @Router /clinic/api/v1/role [POST]
// @Summary Create Roles
// @Description Create Roles
// @Tags Role
// @Accept json
// @Role json
// @Param Role body models.RoleCreate true "CreateRoleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateRole(c *gin.Context) {
	var (
		roleCreate models.RoleCreate
	)

	err := c.ShouldBindJSON(&roleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Role Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Role().Create(c.Request.Context(), &roleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Role.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Role Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Roles godoc
// @ID get_by_id_roles
// @Router /clinic/api/v1/role/{id} [GET]
// @Summary Get By ID Roles
// @Description Get By ID Roles
// @Tags Role
// @Accept json
// @Role json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdRole(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Role().GetByID(c.Request.Context(), &models.RolePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Role.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Role Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Roles godoc
// @ID get_list_roles
// @Router /clinic/api/v1/roles [GET]
// @Summary Get List Roles
// @Description Get List Roles
// @Tags Role
// @Accept json
// @Role json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListRole(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRole INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRole INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Role().GetList(c.Request.Context(), &models.RoleGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Role.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListRole Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Roles godoc
// @ID update_roles
// @Router /clinic/api/v1/role/{id} [PUT]
// @Summary Update Roles
// @Description Update Roles
// @Tags Role
// @Accept json
// @Role json
// @Param id path string true "id"
// @Param Role body models.RoleUpdate true "UpdateRoleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateRole(c *gin.Context) {
	var (
		id         = c.Param("id")
		roleUpdate models.RoleUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&roleUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Role Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	roleUpdate.Id = id
	rowsAffected, err := h.storage.Role().Update(c.Request.Context(), &roleUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Role.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Role.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Role().GetByID(c.Request.Context(), &models.RolePrimaryKey{Id: roleUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Role.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Role Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Roles godoc
// @ID delete_roles
// @Router /clinic/api/v1/role/{id} [DELETE]
// @Summary Delete Roles
// @Description Delete Roles
// @Tags Role
// @Accept json
// @Role json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteRole(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Role().Delete(c.Request.Context(), &models.RolePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Role.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Role Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
