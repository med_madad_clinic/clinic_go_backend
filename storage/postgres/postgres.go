package postgres

import (
	"clinic/config"
	"clinic/pkg/logger"
	"clinic/storage"
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

type store struct {
	db                *pgxpool.Pool
	log               logger.LoggerI
	patients          *patientRepo
	employee          *employeeRepo
	procedure         *procedureRepo
	roles             *roleRepo
	questionnaire     *questionnaireRepo
	salary            *salaryRepo
	expense           *expenseRepo
	payment           *paymentRepo
	category          *categoryRepo
	patientDiagnosis  *patientDiagnosisRepo
	diagnosis         *diagnosisRepo
	schedule          *scheduleRepo
	finishedProcedure *finishedProceduresRepo
	dashboard         *dashboardRepo
	nursePatient      *nursePatientRepo
	consultation      *consultationRepo
	treatment         *treatmentRepo
	treatmentProcedure *treatmentProceduresRepo
}

func NewConnectionPostgres(cfg *config.Config) (storage.StorageI, error) {

	connect, err := pgxpool.ParseConfig(fmt.Sprintf(
		"host=%s user=%s dbname=%s password=%s port=%d sslmode=require",
		cfg.PostgresHost,
		cfg.PostgresUser,
		cfg.PostgresDatabase,
		cfg.PostgresPassword,
		cfg.PostgresPort,
	))

	if err != nil {
		return nil, err
	}
	connect.MaxConns = 30

	pgxpool, err := pgxpool.ConnectConfig(context.Background(), connect)
	if err != nil {
		return nil, err
	}
	var loggerLevel = new(string)
	log := logger.NewLogger("app", *loggerLevel)
	defer func() {
		err := logger.Cleanup(log)
		if err != nil {
			return
		}
	}()
	return &store{
		db:  pgxpool,
		log: logger.NewLogger("app", *loggerLevel),
	}, nil
}

func (s *store) Close() {
	s.db.Close()
}

func (s *store) Patient() storage.PatientI {
	if s.patients == nil {
		s.patients = &patientRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.patients
}

func (s *store) Role() storage.RoleI {
	if s.roles == nil {
		s.roles = &roleRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.roles
}

func (s *store) Employee() storage.EmployeeI {
	if s.employee == nil {
		s.employee = &employeeRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.employee
}

func (s *store) Procedure() storage.ProcedureI {
	if s.procedure == nil {
		s.procedure = &procedureRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.procedure
}

func (s *store) Questionnaire() storage.QuestionnaireI {
	if s.questionnaire == nil {
		s.questionnaire = &questionnaireRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.questionnaire
}

func (s *store) Salary() storage.SalaryI {
	if s.salary == nil {
		s.salary = &salaryRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.salary
}

func (s *store) Expense() storage.ExpenseI {
	if s.expense == nil {
		s.expense = &expenseRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.expense
}

func (s *store) Payment() storage.PaymentI {
	if s.payment == nil {
		s.payment = &paymentRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.payment
}

func (s *store) Category() storage.CategoryI {
	if s.category == nil {
		s.category = &categoryRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.category
}

func (s *store) PatientDiagnosis() storage.PatientDiagnosisI {
	if s.patientDiagnosis == nil {
		s.patientDiagnosis = &patientDiagnosisRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.patientDiagnosis
}

func (s *store) Diagnosis() storage.DiagnosisI {
	if s.diagnosis == nil {
		s.diagnosis = &diagnosisRepo{
			db:  s.db,
			log: s.log,
		}
	}

	return s.diagnosis
}

func (s *store) Schedule() storage.ScheduleI {
	if s.schedule == nil {
		s.schedule = &scheduleRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.schedule
}

func (s *store) FinishedProcedure() storage.FinishedProcedureI {
	if s.finishedProcedure == nil {
		s.finishedProcedure = &finishedProceduresRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.finishedProcedure
}

func (s *store) Dashboard() storage.DashboardI {
	if s.dashboard == nil {
		s.dashboard = &dashboardRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.dashboard
}

func (s *store) NursePatient() storage.NursePatientI {
	if s.nursePatient == nil {
		s.nursePatient = &nursePatientRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.nursePatient
}

func (s *store) Consultation() storage.ConsultationI {
	if s.consultation == nil {
		s.consultation = &consultationRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.consultation
}



func (s *store) Treatment() storage.TreatmentI {
	if s.treatment == nil {
		s.treatment = &treatmentRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.treatment
}

func (s *store) TreatmentProcedure() storage.TreatmentProcedureI {
	if s.treatmentProcedure == nil {
		s.treatmentProcedure = &treatmentProceduresRepo{
			db:  s.db,
			log: s.log,
		}
	}
	return s.treatmentProcedure
}