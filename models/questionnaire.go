package models

type Questionnaire struct {
	Id         string `json:"id,omitempty"`
	Url        string `json:"url,omitempty"`
	Patient_id string `json:"patient_id,omitempty"`
	Created_at string `json:"created_at,omitempty"`
	Updated_at string `json:"updated_at,omitempty"`
	Deleted_at string `json:"deleted_at,omitempty"`
}

type QuestionnaireCreate struct {
	Url        string `json:"url"`
	Patient_id string `json:"patient_id"`
}

type QuestionnaireUpdate struct {
	Id         string `json:"id"`
	Url        string `json:"url"`
}

type QuestionnairePrimaryKey struct {
	Id string `json:"id"`
}

type QuestionnaireGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}



type QuestionnaireGetListResponse struct {
	Count         int              `json:"count"`
	Questionnaire []*Questionnaire `json:"questionnaires"`
}
