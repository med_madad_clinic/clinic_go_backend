package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type treatmentProceduresRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewTreatmentProceduresRepo(db *pgxpool.Pool, log logger.LoggerI) *treatmentProceduresRepo {
	return &treatmentProceduresRepo{
		db:  db,
		log: log,
	}
}

func (u *treatmentProceduresRepo) Create(ctx context.Context, req *models.TreatmentProcedureCreate) (*models.TreatmentProcedure, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO treatment_procedures (
			id,
			treatment_id,
			procedure_id,
			assistant_id,
			time,
			comment,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.TreatmentId, req.ProcedureId, req.AssistantId, req.Time, req.Comment, currentTime)
	if err != nil {
		u.log.Error("error is while creating treatmentProcedures data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.TreatmentProcedurePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id treatmentProcedures" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *treatmentProceduresRepo) GetByID(ctx context.Context, req *models.TreatmentProcedurePrimaryKey) (*models.TreatmentProcedure, error) {
	var (
		query string

		id           sql.NullString
		treatment_id sql.NullString
		procedure_id sql.NullString
		assistant_id sql.NullString
		time         sql.NullString
		comment      sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT 
			id,
			treatment_id,
			procedure_id,
			assistant_id,
			time,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "treatment_procedures" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&treatment_id,
		&procedure_id,
		&assistant_id,
		&time,
		&comment,
		&created_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.TreatmentProcedure{
		Id:          id.String,
		TreatmentId: treatment_id.String,
		ProcedureId: procedure_id.String,
		AssistantId: assistant_id.String,
		Time:        time.String,
		Comment:     comment.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (u *treatmentProceduresRepo) GetList(ctx context.Context, req *models.TreatmentProcedureGetListRequest) (*models.TreatmentProcedureGetListResponse, error) {
	var (
		resp   = &models.TreatmentProcedureGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY tp.created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			tp.id,
			tp.treatment_id,
			tp.procedure_id,
			tp.assistant_id,
			p.name,
			tp.time,
			tp.comment,
			TO_CHAR(tp.created_at, 'dd/mm/yyyy'), 
			TO_CHAR(tp.updated_at, 'dd/mm/yyyy')
		FROM "treatment_procedures" as tp 
		JOIN procedures as p ON p.id = tp.procedure_id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.TreatmentId) > 0 {
		where = fmt.Sprintf(" WHERE treatment_id = '%s' ", req.TreatmentId)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting treatmentProcedures list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			treatment_id   sql.NullString
			procedure_id   sql.NullString
			assistant_id   sql.NullString
			procedure_name sql.NullString
			time           sql.NullString
			comment        sql.NullString
			created_at     sql.NullString
			updated_at     sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&treatment_id,
			&procedure_id,
			&assistant_id,
			&procedure_name,
			&time,
			&comment,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.TreatmentProcedures = append(resp.TreatmentProcedures, &models.TreatmentProcedure{
			Id:            id.String,
			TreatmentId:   treatment_id.String,
			ProcedureId:   procedure_id.String,
			AssistantId:   assistant_id.String,
			ProcedureName: procedure_name.String,
			Time:          time.String,
			Comment:       comment.String,
			CreatedAt:     created_at.String,
			UpdatedAt:     updated_at.String,
		})
	}
	return resp, nil
}

// func (u *treatmentProceduresRepo) Update(ctx context.Context, req *models.TreatmentProceduresUpdate) (int64, error) {
// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			"treatment_procedures"
// 		SET
// 			url = :url,
// 		WHERE id = :id
// 	`

// 	timeZone, err := time.LoadLocation("Asia/Tashkent")
// 	if err != nil {
// 		u.log.Error("error is while loading tashkent timezone" + err.Error())
// 		return 0, err
// 	}

// 	currentTime := time.Now().In(timeZone)

// 	params = map[string]interface{}{
// 		"id":         req.Id,
// 		"url":        req.Url,
// 		"updated_at": currentTime,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	result, err := u.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		u.log.Error("error is while updating treatmentProcedures data", logger.Error(err))
// 		return 0, err
// 	}

// 	return result.RowsAffected(), nil
// }

func (u *treatmentProceduresRepo) Delete(ctx context.Context, req *models.TreatmentProcedurePrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM "treatment_procedures" WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting treatment_procedures", logger.Error(err))
		return err
	}

	return nil
}
