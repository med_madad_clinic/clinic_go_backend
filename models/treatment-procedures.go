package models

type TreatmentProcedure struct {
	Id            string `json:"id,omitempty"`
	TreatmentId   string `json:"treatment_id,omitempty"`
	ProcedureId   string `json:"procedure_id,omitempty"`
	AssistantId   string `json:"assistant_id,omitempty"`
	BloodPressure string `json:"blood_pressure,omitempty"`
	ProcedureName string `json:"procedure_name,omitempty"`
	Time          string `json:"time,omitempty"`
	Comment       string `json:"comment,omitempty"`
	CreatedAt     string `json:"created_at,omitempty"`
	UpdatedAt     string `json:"updated_at,omitempty"`
}

type TreatmentProcedureCreate struct {
	TreatmentId string `json:"-"`
	ProcedureId string `json:"procedure_id"`
	AssistantId string `json:"assistant_id"`
	Time        string `json:"time"`
	Comment     string `json:"comment"`
}

type TreatmentProcedureUpdate struct {
	Time    string `json:"time"`
	Comment string `json:"comment"`
}

type TreatmentProcedurePrimaryKey struct {
	Id string `json:"id"`
}

type TreatmentProcedureGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
	TreatmentId string `json:"treatment_id"`
}

type TreatmentProcedureGetListResponse struct {
	Count               int                   `json:"count"`
	TreatmentProcedures []*TreatmentProcedure `json:"treatment_procedure"`
}
