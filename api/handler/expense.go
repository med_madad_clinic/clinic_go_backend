package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Expenses godoc
// @ID create_expenses
// @Router /clinic/api/v1/expense [POST]
// @Summary Create Expenses
// @Description Create Expenses
// @Tags Expense
// @Accept json
// @Expense json
// @Param Expense body models.ExpenseCreate true "CreateExpenseRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateExpense(c *gin.Context) {
	var (
		expenseCreate models.ExpenseCreate
	)

	err := c.ShouldBindJSON(&expenseCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Expense Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Expense().Create(c.Request.Context(), &expenseCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Expense.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Expense Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Expenses godoc
// @ID get_by_id_expenses
// @Router /clinic/api/v1/expense/{id} [GET]
// @Summary Get By ID Expenses
// @Description Get By ID Expenses
// @Tags Expense
// @Accept json
// @Expense json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdExpense(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Expense().GetByID(c.Request.Context(), &models.ExpensePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Expense.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Expense Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Expenses godoc
// @ID get_list_expenses
// @Router /clinic/api/v1/expenses [GET]
// @Summary Get List Expenses
// @Description Get List Expenses
// @Tags Expense
// @Accept json
// @Expense json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListExpense(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListExpense INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListExpense INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Expense().GetList(c.Request.Context(), &models.ExpenseGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Expense.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListExpense Response!")
	c.JSON(http.StatusOK, resp)
}

// Delete Expenses godoc
// @ID delete_expenses
// @Router /clinic/api/v1/expense/{id} [DELETE]
// @Summary Delete Expenses
// @Description Delete Expenses
// @Tags Expense
// @Accept json
// @Expense json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteExpense(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Expense().Delete(c.Request.Context(), &models.ExpensePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Expense.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Expense Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
