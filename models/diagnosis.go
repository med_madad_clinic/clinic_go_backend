package models

type Diagnosis struct {
	Id            string              `json:"id,omitempty"`
	EmployeeID    string              `json:"employee_id,omitempty"`
	PatientId     string              `json:"patient,omitempty"`
	BloodPressure string              `json:"blood_pressure,omitempty"`
	MKB           string              `json:"mkb,omitempty"`
	MKF           string              `json:"mkf,omitempty"`
	State         string              `json:"state,omitempty"`
	Comment       string              `json:"comment,omitempty"`
	Count         int64               `json:"count,omitempty"`
	Status        string              `json:"status,omitempty"`
	Created_at    string              `json:"created_at,omitempty"`
	Procedure     []*PatientDiagnosis `json:"procedures"`
}

type DiagnosisCreate struct {
	EmployeeID    string                `json:"employee_id,omitempty"`
	PatientId     string                `json:"patient_id,omitempty"`
	BloodPressure string                `json:"blood_pressure,omitempty"`
	MKB           string                `json:"mkb,omitempty"`
	MKF           string                `json:"mkf,omitempty"`
	State         string                `json:"state,omitempty"`
	Comment       string                `json:"comment,omitempty"`
	Count         int64                 `json:"count,omitempty"`
	Procedure     []DiagnosisProcedures `json:"procedures"`
}

type Procedures struct {
	ProcedureId string `json:"procedure_id,omitempty"`
}

type DiagnosisUpdate struct {
	Id         string `json:"id"`
	EmployeeID string `json:"employee_id"`
	PatientId  string `json:"patient_id"`
	Comment    string `json:"comment"`
	Count      int64  `json:"count"`
}

type DiagnosisProcedures struct {
	ProcedureId string `json:"procedure_id"`
	Time        string `json:"time"`
	Comment     string `json:"comment"`
}

type DiagnosisPrimaryKey struct {
	Id string `json:"id"`
}

type DiagnosisGetListRequest struct {
	Offset    int    `json:"offset"`
	Limit     int    `json:"limit"`
	Filter    string `json:"filter"`
	Search    string `json:"search"`
	PatientId string `json:"patient_id"`
}

type DiagnosisGetListResponse struct {
	Count     int          `json:"count"`
	Diagnosis []*Diagnosis `json:"diagnosis"`
}
