package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type diagnosisRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewDiagnosisRepo(db *pgxpool.Pool, log logger.LoggerI) *diagnosisRepo {
	return &diagnosisRepo{
		db:  db,
		log: log,
	}
}

func (u *diagnosisRepo) Create(ctx context.Context, req *models.DiagnosisCreate) (*models.Diagnosis, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO diagnosis (
			id,
			employee_id,
			patient_id,
			blood_pressure,
			mkb,
			mkf,
			state,
			comment,
			count,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query,
		id,
		req.EmployeeID,
		req.PatientId,
		req.BloodPressure,
		req.MKB,
		req.MKF,
		req.State,
		req.Comment,
		req.Count,
		currentTime,
	)
	if err != nil {
		u.log.Error("error is while creating diagnosis data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.DiagnosisPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id diagnosis" + err.Error())
		return nil, err
	}

	query1 := `
			UPDATE consultations
			SET status = 'finished' 
			WHERE patient_id = $1 AND employee_id = $2
	`

	_, err = u.db.Exec(ctx, query1, req.PatientId, req.EmployeeID)
	if err != nil {
		u.log.Error("error is while updating consultation data" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *diagnosisRepo) GetByID(ctx context.Context, req *models.DiagnosisPrimaryKey) (*models.Diagnosis, error) {
	var (
		query          string
		id             sql.NullString
		employee_id    sql.NullString
		patient_id     sql.NullString
		blood_pressure sql.NullString
		mkb            sql.NullString
		mkf            sql.NullString
		state          sql.NullString
		comment        sql.NullString
		count          sql.NullInt64
		created_at     sql.NullString
	)

	query = `
		SELECT 
			id,
			employee_id,
			patient_id,
			blood_pressure,
			mkb,
			mkf,
			state,
			comment,
			count,
			TO_CHAR(created_at,'dd/mm/yyyy') 
		FROM "diagnosis" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&employee_id,
		&patient_id,
		&blood_pressure,
		&mkb,
		&mkf,
		&state,
		&comment,
		&count,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Diagnosis{
		Id:            id.String,
		EmployeeID:    employee_id.String,
		PatientId:     patient_id.String,
		BloodPressure: blood_pressure.String,
		MKB:           mkb.String,
		MKF:           mkf.String,
		State:         state.String,
		Comment:       comment.String,
		Count:         count.Int64,
		Created_at:    created_at.String,
	}, nil
}

func (u *diagnosisRepo) GetList(ctx context.Context, req *models.DiagnosisGetListRequest) (*models.DiagnosisGetListResponse, error) {
	var (
		resp   = &models.DiagnosisGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			employee_id,
			patient_id,
			blood_pressure,
			mkb,
			mkf,
			state,
			comment,
			count,
			TO_CHAR(created_at, 'dd/mm/yyyy') 
		FROM "diagnosis" 
	`
	if len(req.PatientId) > 0 {
		where = fmt.Sprintf(" WHERE patient_id = '%s' ", req.PatientId)
		offset = " OFFSET 0"
		limit = " LIMIT 100"
	}

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting diagnosis list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			employee_id    sql.NullString
			patient_id     sql.NullString
			blood_pressure sql.NullString
			mkb            sql.NullString
			mkf            sql.NullString
			state          sql.NullString
			comment        sql.NullString
			count          sql.NullInt64
			created_at     sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&employee_id,
			&patient_id,
			&blood_pressure,
			&mkb,
			&mkf,
			&state,
			&comment,
			&count,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Diagnosis = append(resp.Diagnosis, &models.Diagnosis{
			Id:            id.String,
			EmployeeID:    employee_id.String,
			PatientId:     patient_id.String,
			BloodPressure: blood_pressure.String,
			MKB:           mkb.String,
			MKF:           mkf.String,
			State:         state.String,
			Comment:       comment.String,
			Count:         count.Int64,
			Created_at:    created_at.String,
		})
	}
	return resp, nil
}

func (u *diagnosisRepo) Update(ctx context.Context, req *models.DiagnosisUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"diagnosis"
		SET
			employee_id = :employee_id,
			patient_id = :patient_id,
			comment = :comment,
			count = :count,
			update = :updated_at
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":          req.Id,
		"employee_id": req.EmployeeID,
		"patient_id":  req.PatientId,
		"comment":     req.Comment,
		"count":       req.Count,
		"updated_at":  currentTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating diagnosis data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *diagnosisRepo) Delete(ctx context.Context, req *models.DiagnosisPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM diagnosis WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting diagnosis", logger.Error(err))
		return err
	}

	return nil
}
