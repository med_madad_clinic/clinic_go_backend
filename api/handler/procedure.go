package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Procedures godoc
// @ID create_procedures
// @Router /clinic/api/v1/procedure [POST]
// @Summary Create Procedures
// @Description Create Procedures
// @Tags Procedure
// @Accept json
// @Procedure json
// @Param Procedure body models.ProcedureCreate true "CreateProcedureRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateProcedure(c *gin.Context) {
	var (
		procedureCreate models.ProcedureCreate
	)

	err := c.ShouldBindJSON(&procedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Procedure Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Procedure().Create(c.Request.Context(), &procedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Procedure.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Procedure Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Procedures godoc
// @ID get_by_id_procedures
// @Router /clinic/api/v1/procedure/{id} [GET]
// @Summary Get By ID Procedures
// @Description Get By ID Procedures
// @Tags Procedure
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdProcedure(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Procedure().GetByID(c.Request.Context(), &models.ProcedurePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Procedure Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Procedures godoc
// @ID get_list_procedures
// @Router /clinic/api/v1/procedures [GET]
// @Summary Get List Procedures
// @Description Get List Procedures
// @Tags Procedure
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListProcedure(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListProcedure INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListProcedure INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Procedure().GetList(c.Request.Context(), &models.ProcedureGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListProcedure Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Procedures by category godoc
// @ID get_list_procedures by category
// @Router /clinic/api/v1/proceduresbycategory [GET]
// @Summary Get List Procedures by category
// @Description Get List Procedures by category
// @Tags Procedure
// @Accept json
// @Procedure json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListProcedureByCategory(c *gin.Context) {
	resp,err := h.storage.Procedure().GetProcdureByCategory(c.Request.Context())
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.GetProcdureByCategory!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	h.logger.Info("GetListProcedureByCategory Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Procedures godoc
// @ID update_procedures
// @Router /clinic/api/v1/procedure/{id} [PUT]
// @Summary Update Procedures
// @Description Update Procedures
// @Tags Procedure
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Procedure body models.ProcedureUpdate true "UpdateProcedureRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateProcedure(c *gin.Context) {
	var (
		id              = c.Param("id")
		procedureUpdate models.ProcedureUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&procedureUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Procedure Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	procedureUpdate.Id = id
	rowsAffected, err := h.storage.Procedure().Update(c.Request.Context(), &procedureUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Procedure.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Procedure().GetByID(c.Request.Context(), &models.ProcedurePrimaryKey{Id: procedureUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Procedure Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Procedures godoc
// @ID delete_procedures
// @Router /clinic/api/v1/procedure/{id} [DELETE]
// @Summary Delete Procedures
// @Description Delete Procedures
// @Tags Procedure
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteProcedure(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Procedure().Delete(c.Request.Context(), &models.ProcedurePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Procedure.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Procedure Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
