package helper

import (
	"clinic/models"
	"context"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"os"

	firebase "firebase.google.com/go"
	"github.com/google/uuid"
	"google.golang.org/api/option"
)

func UploadFiles(file *multipart.Form, name string) (*models.MultipleFileUploadResponse, error) {

	i := 0
	var resp models.MultipleFileUploadResponse

	for _, v := range file.File["file"] {
		id := uuid.New().String()
		var (
			url           models.Url
			
		)

		url.Url = fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/ibron-b5f1c.appspot.com/o/%s?alt=media&token=%s", id, id)

		resp.Url = append(resp.Url, &url)

		imageFile, err := v.Open()
		if err != nil {
			return nil, err
		}
		tempFile, err := os.Create(id)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(tempFile, imageFile)
		if err != nil {
			return nil, err
		}

		defer tempFile.Close()

		opt := option.WithCredentialsFile("serviceAccountKey.json")
		app, err := firebase.NewApp(context.Background(), nil, opt)
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}

		client, err := app.Storage(context.TODO())
		if err != nil {
			return nil, err
		}

		bucketHandle, err := client.Bucket("clinic-7fdab.appspot.com")
		if err != nil {
			return nil, err
		}

		f, err := os.Open(tempFile.Name())
		if err != nil {
			return nil, err
		}
		defer os.Remove(tempFile.Name())
		objectHandle := bucketHandle.Object(tempFile.Name())

		writer := objectHandle.NewWriter(context.Background())

		writer.ObjectAttrs.Metadata = map[string]string{"firebaseStorageDownloadTokens": id}

		defer writer.Close()

		if _, err := io.Copy(writer, f); err != nil {
			return nil, err
		}


		defer os.Remove(tempFile.Name())
		i++
	}

	return &resp, nil
}
