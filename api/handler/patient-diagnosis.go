package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create PatientDiagnosiss godoc
// @ID create_patientDiagnosiss
// @Router /clinic/api/v1/patientDiagnosis [POST]
// @Summary Create PatientDiagnosiss
// @Description Create PatientDiagnosiss
// @Tags PatientDiagnosis
// @Accept json
// @PatientDiagnosis json
// @Param PatientDiagnosis body models.PatientDiagnosisCreate true "CreatePatientDiagnosisRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreatePatientDiagnosis(c *gin.Context) {
	var (
		patientDiagnosisCreate models.PatientDiagnosisCreate
	)

	err := c.ShouldBindJSON(&patientDiagnosisCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error PatientDiagnosis Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.PatientDiagnosis().Create(c.Request.Context(), &patientDiagnosisCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error PatientDiagnosis.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create PatientDiagnosis Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID PatientDiagnosiss godoc
// @ID get_by_id_patientDiagnosiss
// @Router /clinic/api/v1/patientDiagnosis/{id} [GET]
// @Summary Get By ID PatientDiagnosiss
// @Description Get By ID PatientDiagnosiss
// @Tags PatientDiagnosis
// @Accept json
// @PatientDiagnosis json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIDPatientDiagnosis(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.PatientDiagnosis().GetByID(c.Request.Context(), &models.PatientDiagnosisPrimaryKey{ID: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID PatientDiagnosis Response!")
	c.JSON(http.StatusOK, request)
}

// GetList PatientDiagnosiss godoc
// @ID get_list_patientDiagnosiss
// @Router /clinic/api/v1/patientDiagnosiss [GET]
// @Summary Get List PatientDiagnosiss
// @Description Get List PatientDiagnosiss
// @Tags PatientDiagnosis
// @Accept json
// @PatientDiagnosis json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListPatientDiagnosis(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatientDiagnosis INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListPatientDiagnosis INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.PatientDiagnosis().GetList(c.Request.Context(), &models.PatientDiagnosisGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListPatientDiagnosis Response!")
	c.JSON(http.StatusOK, resp)
}

// Update PatientDiagnosiss godoc
// @ID update_patientDiagnosiss
// @Router /clinic/api/v1/patientDiagnosis/{id} [PUT]
// @Summary Update PatientDiagnosiss
// @Description Update PatientDiagnosiss
// @Tags PatientDiagnosis
// @Accept json
// @PatientDiagnosis json
// @Param id path string true "id"
// @Param PatientDiagnosis body models.PatientDiagnosisUpdate true "UpdatePatientDiagnosisRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// // @Failure 500 {object} Response{data=string} "Server error"
// func (h *handler) UpdatePatientDiagnosis(c *gin.Context) {
// 	var (
// 		id                     = c.Param("id")
// 		patientDiagnosisUpdate models.PatientDiagnosisUpdate
// 	)

// 	if !helper.IsValidUUID(id) {
// 		h.logger.Error("is invalid uuid!")
// 		c.JSON(http.StatusBadRequest, "invalid id")
// 		return
// 	}

// 	err := c.ShouldBindJSON(&patientDiagnosisUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "error PatientDiagnosis Should Bind Json!")
// 		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
// 		return
// 	}

// 	patientDiagnosisUpdate.ID = id
// 	rowsAffected, err := h.storage.PatientDiagnosis().Update(c.Request.Context(), &patientDiagnosisUpdate)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.Update!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	if rowsAffected <= 0 {
// 		h.logger.Error("storage.PatientDiagnosis.Update!")
// 		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
// 		return
// 	}

// 	resp, err := h.storage.PatientDiagnosis().GetByID(c.Request.Context(), &models.PatientDiagnosisPrimaryKey{ID: patientDiagnosisUpdate.ID})
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.GetByID!")
// 		c.JSON(http.StatusInternalServerError, "Server Error!")
// 		return
// 	}

// 	h.logger.Info("Update PatientDiagnosis Successfully!")
// 	c.JSON(http.StatusAccepted, resp)
// }

// Delete PatientDiagnosiss godoc
// @ID delete_patientDiagnosiss
// @Router /clinic/api/v1/patientDiagnosis/{id} [DELETE]
// @Summary Delete PatientDiagnosiss
// @Description Delete PatientDiagnosiss
// @Tags PatientDiagnosis
// @Accept json
// @PatientDiagnosis json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeletePatientDiagnosis(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.PatientDiagnosis().Delete(c.Request.Context(), &models.PatientDiagnosisPrimaryKey{ID: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.PatientDiagnosis.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("PatientDiagnosis Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
