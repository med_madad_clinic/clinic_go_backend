package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Consultations godoc
// @ID create_consultations
// @Router /clinic/api/v1/consultation [POST]
// @Summary Create Consultations
// @Description Create Consultations
// @Tags Consultation
// @Accept json
// @Consultation json
// @Param Consultation body models.ConsultationCreate true "CreateConsultationRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateConsultation(c *gin.Context) {
	var (
		consultationCreate models.ConsultationCreate
	)

	err := c.ShouldBindJSON(&consultationCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Consultation Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Consultation().Create(c.Request.Context(), &consultationCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Consultation.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Consultation Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Consultations godoc
// @ID get_by_id_consultations
// @Router /clinic/api/v1/consultation/{id} [GET]
// @Summary Get By ID Consultations
// @Description Get By ID Consultations
// @Tags Consultation
// @Accept json
// @Consultation json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdConsultation(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Consultation().GetByID(c.Request.Context(), &models.ConsultationPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Consultation.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Consultation Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Consultations godoc
// @ID get_list_consultations
// @Router /clinic/api/v1/consultations [GET]
// @Summary Get List Consultations
// @Description Get List Consultations
// @Tags Consultation
// @Accept json
// @Consultation json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListConsultation(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListConsultation INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListConsultation INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Consultation().GetList(c.Request.Context(), &models.ConsultationGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Consultation.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListConsultation Response!")
	c.JSON(http.StatusOK, resp)
}


// Delete Consultations godoc
// @ID delete_consultations
// @Router /clinic/api/v1/consultation/{id} [DELETE]
// @Summary Delete Consultations
// @Description Delete Consultations
// @Tags Consultation
// @Accept json
// @Consultation json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteConsultation(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Consultation().Delete(c.Request.Context(), &models.ConsultationPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Consultation.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Consultation Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
