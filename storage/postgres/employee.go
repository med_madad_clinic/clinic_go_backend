package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
)

type employeeRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewEmployeeRepo(db *pgxpool.Pool, log logger.LoggerI) *employeeRepo {
	return &employeeRepo{
		db:  db,
		log: log,
	}
}

func (u *employeeRepo) Create(ctx context.Context, req *models.EmployeeCreate) (*models.Employee, error) {

	var (
		id       = uuid.New().String()
		query    string
		birthday string
	)

	birthday += req.Birthday[:4]
	birthday += req.Birthday[5:7]
	birthday += req.Birthday[8:10]

	fmt.Println(birthday)

	password, err := bcrypt.GenerateFromPassword([]byte(birthday), 5)
	if err != nil {
		u.log.Error("error generating password: " + err.Error())
		return nil, err
	}

	query = `
		INSERT INTO employees (
			id,
			role_id,
			full_name,
			phone,
			login,
			password,
			balance,
			birthday,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6, $7, $8,$9)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Role_id, req.Full_name, req.Phone, req.Phone, password, 0, req.Birthday, currentTime)
	if err != nil {
		u.log.Error("error is while creating employee data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.EmployeePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id employee" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *employeeRepo) GetByID(ctx context.Context, req *models.EmployeePrimaryKey) (*models.Employee, error) {
	var (
		query      string
		id         sql.NullString
		role_id    sql.NullString
		full_name  sql.NullString
		phone      sql.NullString
		birthday   sql.NullString
		create_at  sql.NullString
		updated_at sql.NullString
		balance    sql.NullFloat64
	)

	query = `
		SELECT 
			id,
			role_id,
			full_name,
			phone,
			birthday,
			balance,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "employees" 
		WHERE deleted_at is null AND id = $1 OR deleted_at is null AND login = $1::text

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&role_id,
		&full_name,
		&phone,
		&birthday,
		&balance,
		&create_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data: " + err.Error())
		return nil, err
	}

	return &models.Employee{
		Id:         id.String,
		Full_name:  full_name.String,
		Birthday:   birthday.String,
		Phone:      phone.String,
		Balance:    balance.Float64,
		Create_at:  create_at.String,
		Updated_at: updated_at.String,
	}, nil
}

func (u *employeeRepo) GetByLogin(ctx context.Context, req *models.EmployeePrimaryKey) (*models.Employee, error) {
	var (
		query      string
		id         sql.NullString
		role_id    sql.NullString
		full_name  sql.NullString
		phone      sql.NullString
		birthday   sql.NullString
		create_at  sql.NullString
		updated_at sql.NullString
		balance    sql.NullFloat64
		password   sql.NullString
	)

	query = `
		SELECT 
			id,
			role_id,
			full_name,
			phone,
			birthday,
			balance,
			password,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "employees" 
		WHERE deleted_at is null AND login = $1::text

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&role_id,
		&full_name,
		&phone,
		&birthday,
		&balance,
		&password,
		&create_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data: " + err.Error())
		return nil, err
	}

	return &models.Employee{
		Id:         id.String,
		Full_name:  full_name.String,
		Role_id:    role_id.String,
		Birthday:   birthday.String,
		Phone:      phone.String,
		Balance:    balance.Float64,
		Password:   password.String,
		Create_at:  create_at.String,
		Updated_at: updated_at.String,
	}, nil
}

func (u *employeeRepo) GetListNurse(ctx context.Context) (*models.EmployeeGetListResponse, error) {
	query := `SELECT
				COUNT(*) OVER(),
				id,
				full_name
			FROM "employees"
			WHERE deleted_at is null AND role_id = (SELECT id FROM roles WHERE role_name = 'Hamshira')

	`
	resp := &models.EmployeeGetListResponse{}

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting employee list" + err.Error())
		return nil, err
	}
	for rows.Next() {
		var (
			id        sql.NullString
			full_name sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&full_name,
		)
		if err != nil {
			u.log.Error("error is while scanning data" + err.Error())
			return nil, err
		}

		resp.Employee = append(resp.Employee, &models.Employee{
			Id:        id.String,
			Full_name: full_name.String,
		})
	}

	return resp, nil

}

func (u *employeeRepo) GetList(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error) {
	var (
		resp   = &models.EmployeeGetListResponse{}
		query  string
		where  = " WHERE TRUE AND deleted_at is null "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			role_id,
			full_name,
			phone,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "employees" 
		
	`

	if len(req.Role) > 0 {
		where += "AND role_id = '" + req.Role + "'"
	}

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting employee list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			role_id    sql.NullString
			full_name  sql.NullString
			phone      sql.NullString
			birthday   sql.NullString
			create_at  sql.NullString
			updated_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&role_id,
			&full_name,
			&phone,
			&birthday,
			&create_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Employee = append(resp.Employee, &models.Employee{
			Id:         id.String,
			Full_name:  full_name.String,
			Birthday:   birthday.String,
			Phone:      phone.String,
			Create_at:  create_at.String,
			Updated_at: updated_at.String,
			Role_id:    role_id.String,
			Order:      order,
		})
		order++
	}
	return resp, nil
}

func (u *employeeRepo) GetListDeletedEmployee(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error) {
	var (
		resp   = &models.EmployeeGetListResponse{}
		query  string
		where  = " WHERE TRUE AND deleted_at is not null"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			role_id,
			full_name,
			phone,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy'),
			TO_CHAR(deleted_at,'dd/mm/yyyy')
		FROM "employees" 
		
	`

	if len(req.Role) > 0 {
		where += "AND role_id = '" + req.Role + "'"
	}

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting employee list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			role_id    sql.NullString
			full_name  sql.NullString
			phone      sql.NullString
			birthday   sql.NullString
			create_at  sql.NullString
			updated_at sql.NullString
			deleted_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&role_id,
			&full_name,
			&phone,
			&birthday,
			&create_at,
			&updated_at,
			&deleted_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Employee = append(resp.Employee, &models.Employee{
			Id:         id.String,
			Full_name:  full_name.String,
			Birthday:   birthday.String,
			Phone:      phone.String,
			Create_at:  create_at.String,
			Updated_at: updated_at.String,
			Role_id:    role_id.String,
			Order:      order,
			Deleted_at: deleted_at.String,
		})
		order++
	}
	return resp, nil
}

func (u *employeeRepo) Update(ctx context.Context, req *models.EmployeeUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"employees"
		SET
			full_name = :full_name,
			phone = :phone,
			birthday = :birthday,
			update = :updated_at
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":         req.Id,
		"full_name":  req.Full_name,
		"birthday":   req.Birthday,
		"phone":      req.Phone,
		"updated_at": currentTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating employee data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *employeeRepo) Delete(ctx context.Context, req *models.EmployeePrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE employees SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting employees", logger.Error(err))
		return err
	}

	return nil
}

func (u *employeeRepo) GetListDoctors(ctx context.Context, req *models.EmployeeGetListRequest) (*models.EmployeeGetListResponse, error) {
	var (
		resp   = &models.EmployeeGetListResponse{}
		query  string
		where  = " WHERE TRUE AND roles.role_name = 'Shifokor' AND e.deleted_at is null"
		offset = " OFFSET 0"
		limit  = " LIMIT 100"
		filter = " ORDER BY e.created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			e.id,
			e.role_id,
			e.full_name,
			e.phone,
			TO_CHAR(e.birthday, 'yyyy-mm-dd'),
			TO_CHAR(e.created_at,'dd/mm/yyyy'), 
			TO_CHAR(e.updated_at,'dd/mm/yyyy')
		FROM "employees" AS e
		JOIN "roles" ON e.role_id = roles.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting employee list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			role_id    sql.NullString
			full_name  sql.NullString
			phone      sql.NullString
			birthday   sql.NullString
			create_at  sql.NullString
			updated_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&role_id,
			&full_name,
			&phone,
			&birthday,
			&create_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Employee = append(resp.Employee, &models.Employee{
			Id:         id.String,
			Full_name:  full_name.String,
			Birthday:   birthday.String,
			Phone:      phone.String,
			Create_at:  create_at.String,
			Updated_at: updated_at.String,
			Order:      order,
		})
		order++
	}
	return resp, nil
}

func (u *employeeRepo) UpdateEmlpoyeeBalance(ctx context.Context, req *models.EmployeePrimaryKey) (int64, error) {

	query := `
		UPDATE employees
		SET balance = employees.balance + $1
		WHERE id = $2
	`

	result, err := u.db.Exec(ctx, query, req.Amount, req.Id)
	if err != nil {
		u.log.Error("error is while updating employee balance", logger.Error(err))
		return 0, err
	}
	return result.RowsAffected(), nil

}
