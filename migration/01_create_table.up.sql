CREATE TABLE IF NOT EXISTS "patients"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR(255) NOT NULL,
    "birthday" DATE NOT NULL,
    "gender" VARCHAR(10) NOT NULL,
    "age" INT NOT NULL,
    "height" VARCHAR NOT NULL,
    "weight" VARCHAR NOT NULL,
    "phone" VARCHAR(13) NOT NULL,
    "home_phone" VARCHAR(13) NOT NULL,
    "address" VARCHAR(255) NOT NULL,
    "apartment_number"  VARCHAR(10) NOT NULL,
    "latitude" VARCHAR NOT NULL,
    "longitude" VARCHAR NOT NULL,
    "enterance" VARCHAR(10) NOT NULL,
    "floor" VARCHAR(10) NOT NULL,
    "home_number" VARCHAR(10) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "roles"(
    "id" UUID PRIMARY KEY,
    "role_name" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP NOT NULL,
    "updated_at" TIMESTAMP
);

--crud qlish kere
CREATE TABLE IF NOT EXISTS "categories"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "employees"(
    "id" UUID PRIMARY KEY,
    "role_id" UUID REFERENCES "roles"("id"),
    "full_name" VARCHAR(255) NOT NULL,
    "birthday" DATE NOT NULL,
    "phone" VARCHAR(13) NOT NULL,
    "login" VARCHAR(255) NOT NULL, 
    "password" VARCHAR(255) NOT NULL, 
    "balance" NUMERIC NOT NULL, 
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "procedures"(
    "id" UUID PRIMARY KEY,
    "category_id" UUID REFERENCES "categories"("id"), --crudga qoshish
    "name" VARCHAR(255) NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    "nurse_share" NUMERIC NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "questionnaires"(
    "id" UUID PRIMARY KEY,
    "url" VARCHAR(255) NOT NULL,
    "patient_id" UUID REFERENCES "patients"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "salary"(
    "id" UUID PRIMARY KEY,
    "employee_id" UUID REFERENCES "employees"("id"),
    "cash" NUMERIC NOT NULL,
    "card" NUMERIC NOT NULL,
    "comment" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "expenses"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "employee_id" UUID REFERENCES "employees"("id"),
    "cash" NUMERIC NOT NULL,
    "card" NUMERIC NOT NULL,
    "comment" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "payments"(
    "id" UUID PRIMARY KEY,
    "patient_id" UUID REFERENCES "patients"("id"),
    "cash" NUMERIC NOT NULL,
    "card" NUMERIC NOT NULL,
    "comment" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "diagnosis"(
    "id" UUID PRIMARY KEY,
    "employee_id" UUID REFERENCES "employees"("id"),
    "patient_id" UUID REFERENCES "patients"("id"),
    "blood_pressure" VARCHAR(255) NOT NULL,
    "mkb" VARCHAR NOT NULL,
    "mkf" VARCHAR NOT NULL,
    "state" VARCHAR(255) NOT NULL,
    "comment" VARCHAR(255) NOT NULL,
    "count" INT NOT NULL,
    "status" VARCHAR DEFAULT 'ongoing' NOT NULL CHECK ("status" IN ('ongoing', 'canceled','finished')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "patient_procedure"(
    "id" UUID PRIMARY KEY,
    "patient_id" UUID REFERENCES "patients"("id"),
    "procedure_id" UUID REFERENCES "procedures"("id"),
    "diagnosis_id" UUID REFERENCES "diagnosis"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "schedule"(
    "id" UUID PRIMARY KEY,
    "patient_id" UUID REFERENCES "patients"("id"),
    "employee_id" UUID REFERENCES "employees"("id"),
    "start_date" DATE NOT NULL,
    "day" VARCHAR NOT NULL,
    "start_time" TIME NOT NULL,
    "end_time" TIME,
    "status" VARCHAR DEFAULT 'active' NOT NULL CHECK ("status" IN ('active', 'canceled')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
   

CREATE TABLE IF NOT EXISTS "finished_procedures"(
    "id" UUID PRIMARY KEY,
    "procedure_id"  UUID REFERENCES "procedures"("id"),
    "diagnosis_id" UUID REFERENCES "diagnosis"("id"),
    "patient_id" UUID REFERENCES "patients"("id"),
    "employee_id" UUID REFERENCES "employees"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "nurse_patients"(
    "id" UUID PRIMARY KEY,
    "employee_id" UUID REFERENCES "employees"("id"),
    "patient_id" UUID REFERENCES "patients"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "consultations"(
    "id" UUID PRIMARY KEY,
    "patient_id" UUID REFERENCES "patients"("id"),
    "employee_id" UUID REFERENCES "employees"("id"),
    "date" DATE NOT NULL,
    "start_time" TIME NOT NULL,
    "end_time" TIME,
    "type" VARCHAR CHECK ("type" IN ('online', 'offline')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "treatment"(
    "id" UUID PRIMARY KEY,
    "diagnosis_id" UUID REFERENCES "diagnosis"("id"),
    "employee_id" UUID REFERENCES "employees"("id"),
    "blood_pressure" VARCHAR(255) NOT NULL,
    "status" VARCHAR DEFAULT 'new' NOT NULL CHECK ("status" IN ('new', 'canceled','finished')),
    "count" INT NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "treatment_procedures"(
    "id" UUID PRIMARY KEY,
    "treatment_id" UUID REFERENCES "treatment"("id"),
    "procedure_id" UUID REFERENCES "procedures"("id"),
    "assistant_id" UUID REFERENCES "employees"("id"),
    "time" TIME NOT NULL,
    "comment" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);






