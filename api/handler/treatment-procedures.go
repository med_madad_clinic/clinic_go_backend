package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create TreatmentProcedures godoc
// @ID create_reatmentprocedures
// @Router /clinic/api/v1/reatmentprocedure [POST]
// @Summary Create TreatmentProcedures
// @Description Create TreatmentProcedures
// @Tags TreatmentProcedure
// @Accept json
// @TreatmentProcedure json
// @Param TreatmentProcedure body models.TreatmentProcedureCreate true "CreateTreatmentProcedureRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateTreatmentProcedure(c *gin.Context) {
	var (
		reatmentprocedureCreate models.TreatmentProcedureCreate
	)

	err := c.ShouldBindJSON(&reatmentprocedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error TreatmentProcedure Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.TreatmentProcedure().Create(c.Request.Context(), &reatmentprocedureCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error TreatmentProcedure.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create TreatmentProcedure Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID TreatmentProcedures godoc
// @ID get_by_id_reatmentprocedures
// @Router /clinic/api/v1/reatmentprocedure/{id} [GET]
// @Summary Get By ID TreatmentProcedures
// @Description Get By ID TreatmentProcedures
// @Tags TreatmentProcedure
// @Accept json
// @TreatmentProcedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdTreatmentProcedure(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.TreatmentProcedure().GetByID(c.Request.Context(), &models.TreatmentProcedurePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.TreatmentProcedure.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID TreatmentProcedure Response!")
	c.JSON(http.StatusOK, request)
}

// GetList TreatmentProcedures godoc
// @ID get_list_reatmentprocedures
// @Router /clinic/api/v1/reatmentprocedures [GET]
// @Summary Get List TreatmentProcedures
// @Description Get List TreatmentProcedures
// @Tags TreatmentProcedure
// @Accept json
// @TreatmentProcedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListTreatmentProcedure(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListTreatmentProcedure INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListTreatmentProcedure INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.TreatmentProcedure().GetList(c.Request.Context(), &models.TreatmentProcedureGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.TreatmentProcedure.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListTreatmentProcedure Response!")
	c.JSON(http.StatusOK, resp)
}

// Delete TreatmentProcedures godoc
// @ID delete_reatmentprocedures
// @Router /clinic/api/v1/reatmentprocedure/{id} [DELETE]
// @Summary Delete TreatmentProcedures
// @Description Delete TreatmentProcedures
// @Tags TreatmentProcedure
// @Accept json
// @TreatmentProcedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteTreatmentProcedure(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.TreatmentProcedure().Delete(c.Request.Context(), &models.TreatmentProcedurePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.TreatmentProcedure.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("TreatmentProcedure Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
