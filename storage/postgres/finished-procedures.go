package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type finishedProceduresRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewFinishedProceduresRepo(db *pgxpool.Pool, log logger.LoggerI) *finishedProceduresRepo {
	return &finishedProceduresRepo{
		db:  db,
		log: log,
	}
}

func (u *finishedProceduresRepo) Create(ctx context.Context, req *models.FinishedProceduresCreate) (*models.FinishedProcedures, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO finished_procedures (
			id
			diagnosis_id
			patient_id
			employee_id
			created_at
		)
		VALUES($1, $2, $3, $4, $5)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.DiagnosisId, req.PatientId, req.EmployeeId, currentTime)
	if err != nil {
		u.log.Error("error is while creating finishedProcedures data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.FinishedProceduresPrimaryKey{ID: id})
	if err != nil {
		u.log.Error("error get by id finishedProcedures" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *finishedProceduresRepo) GetByID(ctx context.Context, req *models.FinishedProceduresPrimaryKey) (*models.FinishedProcedures, error) {
	var (
		query        string
		id           sql.NullString
		diagnosis_id sql.NullString
		patient_id   sql.NullString
		employee_id  sql.NullString
		created_at   sql.NullString
	)

	query = `
		SELECT 
			id,
			diagnosis_id,
			patient_id,
			employee_id,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "finished_procedures" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.ID).Scan(
		&id,
		&patient_id,
		&diagnosis_id,
		&employee_id,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.FinishedProcedures{
		Id:          id.String,
		PatientId:   patient_id.String,
		DiagnosisId: diagnosis_id.String,
		EmployeeId:  employee_id.String,
		CreatedAt:   created_at.String,
	}, nil
}

func (u *finishedProceduresRepo) GetList(ctx context.Context, req *models.FinishedProceduresGetListRequest) (*models.FinishedProceduresGetListResponse, error) {
	var (
		resp   = &models.FinishedProceduresGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			diagnosis_id,
			patient_id,
			employee_id,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "finished_procedures"
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting finishedProcedures list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			diagnosis_id sql.NullString
			patient_id   sql.NullString
			employee_id  sql.NullString
			created_at   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&patient_id,
			&diagnosis_id,
			&employee_id,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.FinishedProcedures = append(resp.FinishedProcedures, models.FinishedProcedures{
			Id:          id.String,
			PatientId:   patient_id.String,
			DiagnosisId: diagnosis_id.String,
			EmployeeId:  employee_id.String,
			CreatedAt:   created_at.String,
		})
	}
	return resp, nil
}

// func (u *finishedProceduresRepo) Update(ctx context.Context, req *models.FinishedProceduresUpdate) (int64, error) {
// 	var (
// 		query  string
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			"finishedProcedures"
// 		SET
// 			employee_id = :employee_id,
// 			patient_id = :patient_id,
// 			comment = :comment,
// 			count = :count,
// 			update = :updated_at
// 		WHERE id = :id
// 	`

// 	timeZone, err := time.LoadLocation("Asia/Tashkent")
// 	if err != nil {
// 		u.log.Error("error is while loading tashkent timezone" + err.Error())
// 		return 0, err
// 	}

// 	currentTime := time.Now().In(timeZone)

// 	params = map[string]interface{}{
// 		"id":          req.Id,
// 		"employee_id": req.FinishedProceduresId,
// 		"patient_id":  req.PatientId,
// 		"comment":     req.Comment,
// 		"count":       req.Count,
// 		"updated_at":  currentTime,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)
// 	result, err := u.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		u.log.Error("error is while updating finishedProcedures data", logger.Error(err))
// 		return 0, err
// 	}

// 	return result.RowsAffected(), nil
// }

func (u *finishedProceduresRepo) Delete(ctx context.Context, req *models.FinishedProceduresPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM finished_procedures   WHERE id = $1`, req.ID)
	if err != nil {
		u.log.Error("error is while deleting finishedProcedures", logger.Error(err))
		return err
	}

	return nil
}
