package models

type Expense struct {
	Id          string  `json:"id,omitempty"`
	Employee_id string  `json:"employee_id,omitempty"`
	Employee    string  `json:"employee,omitempty"`
	Name        string  `json:"name,omitempty"`
	Cash        float64 `json:"cash,omitempty"`
	Card        float64 `json:"card,omitempty"`
	Comment     string  `json:"comment,omitempty"`
	Created_at  string  `json:"created_at,omitempty"`
}

type ExpenseCreate struct {
	Employee_id string  `json:"employee_id,omitempty"`
	Name        string  `json:"name,omitempty"`
	Cash        float64 `json:"cash,omitempty"`
	Card        float64 `json:"card,omitempty"`
	Comment     string  `json:"comment,omitempty"`
}

type ExpensePrimaryKey struct {
	Id string `json:"id"`
}

type ExpenseGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type ExpenseGetListResponse struct {
	Count   int        `json:"count"`
	Expense []*Expense `json:"expense"`
}
