package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Treatments godoc
// @ID create_treatments
// @Router /clinic/api/v1/treatment [POST]
// @Summary Create Treatments
// @Description Create Treatments
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param Treatment body models.TreatmentCreate true "CreateTreatmentRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateTreatment(c *gin.Context) {
	var (
		treatmentCreate models.TreatmentCreate
	)

	err := c.ShouldBindJSON(&treatmentCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Treatment Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Treatment().Create(c.Request.Context(), &treatmentCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Treatment.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, procedure := range treatmentCreate.Procedure {
		_, err = h.storage.TreatmentProcedure().Create(c.Request.Context(), &models.TreatmentProcedureCreate{
			TreatmentId: resp.Id,
			ProcedureId: procedure.ProcedureId,
			AssistantId: procedure.AssistantId,
			Time:        procedure.Time,
			Comment:     procedure.Comment,
		})

		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error TreatmentProcedure.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		procedureInfo,err := h.storage.Procedure().GetByID(c.Request.Context(), &models.ProcedurePrimaryKey{
			Id: procedure.ProcedureId,
		})

		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error Procedure.GetByID")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}

		if len(procedure.AssistantId) != 0{
			_,err := h.storage.Employee().UpdateEmlpoyeeBalance(c.Request.Context(), &models.EmployeePrimaryKey{
				Id: procedure.AssistantId,
				Amount: procedureInfo.NurseShare/2,
			})
			if err != nil {
				h.logger.Error(err.Error() + "  :  " + "error Employee.UpdateEmlpoyeeBalance")
				c.JSON(http.StatusInternalServerError, "Server Error!")
				return
			}
			_,err = h.storage.Employee().UpdateEmlpoyeeBalance(c.Request.Context(), &models.EmployeePrimaryKey{
				Id: treatmentCreate.EmployeeId,
				Amount: procedureInfo.NurseShare/2,
			})
			if err != nil {
				h.logger.Error(err.Error() + "  :  " + "error Employee.UpdateEmlpoyeeBalance")
				c.JSON(http.StatusInternalServerError, "Server Error!")
				return
			}
		}else{
			_,err := h.storage.Employee().UpdateEmlpoyeeBalance(c.Request.Context(), &models.EmployeePrimaryKey{
				Id: treatmentCreate.EmployeeId,
				Amount: procedureInfo.NurseShare,
			})
			if err != nil {
				h.logger.Error(err.Error() + "  :  " + "error Employee.UpdateEmlpoyeeBalance")
				c.JSON(http.StatusInternalServerError, "Server Error!")
				return
			}
		}
	}

	h.logger.Info("Create Treatment Successfully!!")
	c.JSON(200, resp)
}

// GetByID Procedures by diagnosis_id godoc
// @ID get_by_id_Procedures
// @Router /clinic/api/v1/diagnosis-procedures [GET]
// @Summary Get By ID Procedures by diagnosis_id
// @Description Get By ID Procedures by diagnosis_id
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param diagnosis_id query string false "diagnosis_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetProcedureByDiagnosisId(c *gin.Context) {
	diagnosis_id := c.Query("diagnosis_id")

	if !helper.IsValidUUID(diagnosis_id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}


	resp,err := h.storage.Treatment().GetDiagnosisProceduresByDiagnosisID(c.Request.Context(),&models.DiagnosisPrimaryKey{Id: diagnosis_id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Treatment.GetDiagnosisProceduresByDiagnosisID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	

	h.logger.Info("GetByID Treatment Response!")
	c.JSON(http.StatusOK, resp)
}

// GetByID Treatments godoc
// @ID get_by_id_treatments
// @Router /clinic/api/v1/treatment/{id} [GET]
// @Summary Get By ID Treatments
// @Description Get By ID Treatments
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdTreatment(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Treatment().GetByID(c.Request.Context(), &models.TreatmentPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Treatment.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	procedures, err := h.storage.TreatmentProcedure().GetList(c.Request.Context(), &models.TreatmentProcedureGetListRequest{
		TreatmentId: id,
		Limit:       1000,
		Offset:      0,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.TreatmentProcedure.GetLIST!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	request.Procedure.Procedures = procedures.TreatmentProcedures
	request.Procedure.BloodPressure = request.BloodPresure

	h.logger.Info("GetByID Treatment Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Treatments godoc
// @ID get_list_treatments
// @Router /clinic/api/v1/treatments [GET]
// @Summary Get List Treatments
// @Description Get List Treatments
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Param diagnosis_id query string false "diagnosis_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListTreatment(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListTreatment INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListTreatment INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Treatment().GetList(c.Request.Context(), &models.TreatmentGetListRequest{
		Offset:      offset,
		Limit:       limit,
		Search:      search,
		DiagnosisId: c.Query("diagnosis_id"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Treatment.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListTreatment Response!")
	c.JSON(http.StatusOK, resp)
}

// Delete Treatments godoc
// @ID delete_treatments
// @Router /clinic/api/v1/treatment/{id} [DELETE]
// @Summary Delete Treatments
// @Description Delete Treatments
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteTreatment(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Treatment().Delete(c.Request.Context(), &models.TreatmentPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Treatment.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Treatment Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// GetByID Treatments Procedures godoc
// @ID get_treatment_procedures
// @Router /clinic/api/v1/treatment-procedures [GET]
// @Summary Get  Treatments Procedures
// @Description Get  Treatments Procedures
// @Tags Treatment
// @Accept json
// @Treatment json
// @Param treatment_id query string false "treatment_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetTreatmentProcedures(c *gin.Context) {
	id := c.Query("treatment_id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.TreatmentProcedure().GetList(c.Request.Context(), &models.TreatmentProcedureGetListRequest{
		TreatmentId: id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.TreatmentProcedure.GetLIST!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	c.JSON(http.StatusOK, request)
}
