package models

type Category struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
}

type CategoryCreate struct {
	Name string `json:"name"`
}

type CategoryUpdate struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type CategoryPrimaryKey struct {
	Id string `json:"id"`
}

type CategoryGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type CategoryGetListResponse struct {
	Count    int         `json:"count"`
	Category []*Category `json:"categories"`
}
