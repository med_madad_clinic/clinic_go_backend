package postgres

import (
	"clinic/models"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type paymentRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewPaymentRepo(db *pgxpool.Pool, log logger.LoggerI) *paymentRepo {
	return &paymentRepo{
		db:  db,
		log: log,
	}
}

func (u *paymentRepo) Create(ctx context.Context, req *models.PaymentCreate) (*models.Payment, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO payment (
			id,
			employee_id,
			cash,
			card,
			comment,
			created_at
		)
		VALUES($1, $2, $3, $4, $5, $6)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Patient_Id, req.Cash, req.Card, req.Comment, currentTime)
	if err != nil {
		u.log.Error("error is while creating payment data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.PaymentPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id payment" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *paymentRepo) GetByID(ctx context.Context, req *models.PaymentPrimaryKey) (*models.Payment, error) {
	var (
		query      string
		id         sql.NullString
		patient_id sql.NullString
		cash       sql.NullFloat64
		card       sql.NullFloat64
		comment    sql.NullString
		created_at sql.NullString
	)

	query = `
		SELECT 
			id,
			patient_id,
			cash,
			card,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy') 
		FROM "payment" 
		WHERE deleted_at is null AND id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&patient_id,
		&cash,
		&card,
		&comment,
		&created_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Payment{
		Id:         id.String,
		Patient_Id: patient_id.String,
		Cash:       cash.Float64,
		Card:       card.Float64,
		Comment:    comment.String,
		CreatedAt:  created_at.String,
	}, nil
}

func (u *paymentRepo) GetList(ctx context.Context, req *models.PaymentGetListRequest) (*models.PaymentGetListResponse, error) {
	var (
		resp   = &models.PaymentGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			employee_id,
			cash,
			card,
			comment,
			TO_CHAR(created_at,'dd/mm/yyyy')
		FROM "payment" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting payment list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			patient_id sql.NullString
			cash       sql.NullFloat64
			card       sql.NullFloat64
			comment    sql.NullString
			created_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&patient_id,
			&cash,
			&card,
			&comment,
			&created_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Payment = append(resp.Payment, &models.Payment{
			Id:         id.String,
			Patient_Id: patient_id.String,
			Cash:       cash.Float64,
			Card:       card.Float64,
			Comment:    comment.String,
			CreatedAt:  created_at.String,
		})
	}
	return resp, nil
}

func (u *paymentRepo) Delete(ctx context.Context, req *models.PaymentPrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE payments SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting payments", logger.Error(err))
		return err
	}

	return nil
}
