package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type roleRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewRoleRepo(db *pgxpool.Pool, log logger.LoggerI) *roleRepo {
	return &roleRepo{
		db:  db,
		log: log,
	}
}

func (u *roleRepo) Create(ctx context.Context, req *models.RoleCreate) (*models.Role, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO roles (
			id,
			role_name,
			created_at
		)
		VALUES($1, $2, $3)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Role_name, currentTime)
	if err != nil {
		u.log.Error("error is while creating role data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.RolePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id role" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *roleRepo) GetByID(ctx context.Context, req *models.RolePrimaryKey) (*models.Role, error) {
	var (
		query      string
		id         sql.NullString
		role_name  sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT 
			id,
			role_name,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "roles" 
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&role_name,
		&created_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Role{
		Id:         id.String,
		Role_name:  role_name.String,
		Created_at: created_at.String,
		Updated_at: updated_at.String,
	}, nil
}

func (u *roleRepo) GetList(ctx context.Context, req *models.RoleGetListRequest) (*models.RoleGetListResponse, error) {
	var (
		resp   = &models.RoleGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
		order  = 1
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			role_name,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "roles" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting role list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			role_name  sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&role_name,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Role = append(resp.Role, &models.Role{
			Id:         id.String,
			Role_name:  role_name.String,
			Created_at: created_at.String,
			Updated_at: updated_at.String,
			Order:      order,
		})
		order++
	}
	return resp, nil
}

func (u *roleRepo) Update(ctx context.Context, req *models.RoleUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"roles"
		SET
			role_name = :role_name,
			update = :updated_at
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":         req.Id,
		"role_name":  req.Role_name,
		"updated_at": currentTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating role data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *roleRepo) Delete(ctx context.Context, req *models.RolePrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM "roles" WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting roles", logger.Error(err))
		return err
	}

	return nil
}
