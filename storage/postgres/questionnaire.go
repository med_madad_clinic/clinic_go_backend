package postgres

import (
	"clinic/models"
	"clinic/pkg/helper"
	"clinic/pkg/logger"
	"context"
	"database/sql"
	"fmt"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type questionnaireRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewQuestionnaireRepo(db *pgxpool.Pool, log logger.LoggerI) *questionnaireRepo {
	return &questionnaireRepo{
		db:  db,
		log: log,
	}
}

func (u *questionnaireRepo) Create(ctx context.Context, req *models.QuestionnaireCreate) (*models.Questionnaire, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO questionnaires (
			id,
			patient_id,
			url,
			created_at
		)
		VALUES($1, $2, $3, $4)
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return nil, err
	}
	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, query, id, req.Patient_id, req.Url, currentTime)
	if err != nil {
		u.log.Error("error is while creating questionnaire data" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.QuestionnairePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id questionnaire" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *questionnaireRepo) GetByID(ctx context.Context, req *models.QuestionnairePrimaryKey) (*models.Questionnaire, error) {
	var (
		query      string
		id         sql.NullString
		url        sql.NullString
		patient_id sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT 
			id,
			url,
			patient_id,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "questionnaires" 
		WHERE deleted_at is null AND id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&url,
		&patient_id,
		&created_at,
		&updated_at,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Questionnaire{
		Id:         id.String,
		Url:        url.String,
		Patient_id: patient_id.String,
		Created_at: created_at.String,
		Updated_at: updated_at.String,
	}, nil
}

func (u *questionnaireRepo) GetList(ctx context.Context, req *models.QuestionnaireGetListRequest) (*models.QuestionnaireGetListResponse, error) {
	var (
		resp   = &models.QuestionnaireGetListResponse{}
		query  string
		where  = " WHERE TRUE "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			role_id,
			full_name,
			phone,
			birthday,
			TO_CHAR(created_at,'dd/mm/yyyy'), 
			TO_CHAR(updated_at,'dd/mm/yyyy')
		FROM "questionnaires" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting questionnaire list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			url        sql.NullString
			patient_id sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&url,
			&patient_id,
			&created_at,
			&updated_at,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Questionnaire = append(resp.Questionnaire, &models.Questionnaire{
			Id:         id.String,
			Url:        url.String,
			Patient_id: patient_id.String,
			Created_at: created_at.String,
			Updated_at: updated_at.String,
		})
	}
	return resp, nil
}

func (u *questionnaireRepo) Update(ctx context.Context, req *models.QuestionnaireUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"questionnaires"
		SET
			url = :url,
		WHERE id = :id
	`

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return 0, err
	}

	currentTime := time.Now().In(timeZone)

	params = map[string]interface{}{
		"id":         req.Id,
		"url":        req.Url,
		"updated_at": currentTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating questionnaire data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *questionnaireRepo) Delete(ctx context.Context, req *models.QuestionnairePrimaryKey) error {

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error is while loading tashkent timezone" + err.Error())
		return err
	}

	currentTime := time.Now().In(timeZone)

	_, err = u.db.Exec(ctx, `UPDATE questionnaires SET deleted_at = $1  WHERE id = $2`, currentTime, req.Id)
	if err != nil {
		u.log.Error("error is while deleting questionnaires", logger.Error(err))
		return err
	}

	return nil
}
