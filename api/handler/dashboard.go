package handler

import "github.com/gin-gonic/gin"

// GetByID AdminDashboards godoc
// @ID get_by_id_dashboards
// @Router /clinic/api/v1/dashboard [GET]
// @Summary Get By ID AdminDashboards
// @Description Get By ID AdminDashboards
// @Tags AdminDashboard
// @Accept json
// @AdminDashboard json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) AdminDashboard(c *gin.Context) {
	resp, err := h.storage.Dashboard().TotalCount(c.Request.Context())
	if err != nil {
		h.logger.Error(err.Error() + "error while getting total count")
		return
	}

	c.JSON(200, resp)

}



// GetByID Today Schedule godoc
// @ID get list today schedule
// @Router /clinic/api/v1/today-schedule [GET]
// @Summary Get By ID Today Schedule
// @Description Get By ID Today Schedule
// @Tags AdminDashboard
// @Accept json
// @AdminDashboard json
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) TodaySchedule(c *gin.Context) {
	date := c.Query("date")
	resp, err := h.storage.Dashboard().ScheduleToday(c.Request.Context(), date)
	if err != nil {
		h.logger.Error(err.Error() + "error while getting today schedule")
		return
	}
	c.JSON(200, resp)
}


// Get List Consultations Today Schedule godoc
// @ID get list Consultations
// @Router /clinic/api/v1/consultations-list [GET]
// @Summary Get list Consultations
// @Description Get list Consultations
// @Tags AdminDashboard
// @Accept json
// @AdminDashboard json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) ConsultationsList(c *gin.Context) {
	resp, err := h.storage.Dashboard().GetListConsultations(c.Request.Context())
	if err != nil {
		h.logger.Error(err.Error() + "error while getting consultations list")
		return
	}
	c.JSON(200, resp)
}



// Get List PatientStatistics Today Schedule godoc
// @ID get list PatientStatistics
// @Router /clinic/api/v1/patient-statistics [GET]
// @Summary Get list PatientStatistics
// @Description Get list PatientStatistics
// @Tags AdminDashboard
// @Accept json
// @AdminDashboard json
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) PatientsStatistics(c *gin.Context) {
	resp,err := h.storage.Dashboard().PatientStatistics(c.Request.Context())
	if err != nil {
		h.logger.Error(err.Error() + "error while getting patient statistics")
		return
	}
	c.JSON(200, resp)
}