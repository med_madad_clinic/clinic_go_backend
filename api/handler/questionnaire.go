package handler

import (
	"clinic/models"
	"clinic/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Questionnaires godoc
// @ID create_questionnaires
// @Router /clinic/api/v1/questionnaire [POST]
// @Summary Create Questionnaires
// @Description Create Questionnaires
// @Tags Questionnaire
// @Accept json
// @Questionnaire json
// @Param Questionnaire body models.QuestionnaireCreate true "CreateQuestionnaireRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateQuestionnaire(c *gin.Context) {
	var (
		questionnaireCreate models.QuestionnaireCreate
	)

	err := c.ShouldBindJSON(&questionnaireCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Questionnaire Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Questionnaire().Create(c.Request.Context(), &questionnaireCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Questionnaire.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Questionnaire Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Questionnaires godoc
// @ID get_by_id_questionnaires
// @Router /clinic/api/v1/questionnaire/{id} [GET]
// @Summary Get By ID Questionnaires
// @Description Get By ID Questionnaires
// @Tags Questionnaire
// @Accept json
// @Questionnaire json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdQuestionnaire(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Questionnaire().GetByID(c.Request.Context(), &models.QuestionnairePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Questionnaire.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Questionnaire Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Questionnaires godoc
// @ID get_list_questionnaires
// @Router /clinic/api/v1/questionnaires [GET]
// @Summary Get List Questionnaires
// @Description Get List Questionnaires
// @Tags Questionnaire
// @Accept json
// @Questionnaire json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListQuestionnaire(c *gin.Context) {

	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListQuestionnaire INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListQuestionnaire INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Questionnaire().GetList(c.Request.Context(), &models.QuestionnaireGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Questionnaire.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListQuestionnaire Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Questionnaires godoc
// @ID update_questionnaires
// @Router /clinic/api/v1/questionnaire/{id} [PUT]
// @Summary Update Questionnaires
// @Description Update Questionnaires
// @Tags Questionnaire
// @Accept json
// @Questionnaire json
// @Param id path string true "id"
// @Param Questionnaire body models.QuestionnaireUpdate true "UpdateQuestionnaireRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateQuestionnaire(c *gin.Context) {
	var (
		id                  = c.Param("id")
		questionnaireUpdate models.QuestionnaireUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&questionnaireUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Questionnaire Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	questionnaireUpdate.Id = id
	rowsAffected, err := h.storage.Questionnaire().Update(c.Request.Context(), &questionnaireUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Questionnaire.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Questionnaire.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Questionnaire().GetByID(c.Request.Context(), &models.QuestionnairePrimaryKey{Id: questionnaireUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Questionnaire.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Questionnaire Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Questionnaires godoc
// @ID delete_questionnaires
// @Router /clinic/api/v1/questionnaire/{id} [DELETE]
// @Summary Delete Questionnaires
// @Description Delete Questionnaires
// @Tags Questionnaire
// @Accept json
// @Questionnaire json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteQuestionnaire(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Questionnaire().Delete(c.Request.Context(), &models.QuestionnairePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Questionnaire.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Questionnaire Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
